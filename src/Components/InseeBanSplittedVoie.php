<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoieInterface;
use Stringable;

/**
 * InseeBanSplittedVoie class file.
 * 
 * This class represents a normalization step for the nom voie.
 * 
 * @author Anastaszor
 */
class InseeBanSplittedVoie implements Stringable
{
	
	/**
	 * The type of voie.
	 * 
	 * @var ApiFrInseeBanTypeVoieInterface
	 */
	protected ApiFrInseeBanTypeVoieInterface $_typeVoie;
	
	/**
	 * The remaining voie data.
	 * 
	 * @var string
	 */
	protected string $_remaining;
	
	/**
	 * Builds a new InseeBanSplittedVoie with the given voie and remaining string.
	 * 
	 * @param ApiFrInseeBanTypeVoieInterface $typeVoie
	 * @param string $remaining
	 */
	public function __construct(ApiFrInseeBanTypeVoieInterface $typeVoie, string $remaining)
	{
		$this->_typeVoie = $typeVoie;
		$this->_remaining = $remaining;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the normalized type voie.
	 * 
	 * @return ApiFrInseeBanTypeVoieInterface
	 */
	public function getTypeVoie() : ApiFrInseeBanTypeVoieInterface
	{
		return $this->_typeVoie;
	}
	
	/**
	 * Gets the remaining of the voie string, minus the type voie.
	 * 
	 * @return string
	 */
	public function getRemaining() : string
	{
		return $this->_remaining;
	}
	
}
