<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use Yii2Module\Yii2InseeBan\Models\InseeBanAddress;

/**
 * InseeBanBayesianAddress class file.
 * 
 * This class represents a search result over an address with its given fitness.
 * 
 * @author Anastaszor
 */
class InseeBanBayesianAddress
{
	
	/**
	 * The group this address is in.
	 * 
	 * @var InseeBanBayesianGroup
	 */
	protected InseeBanBayesianGroup $_bayesianGroup;
	
	/**
	 * The targeted address.
	 * 
	 * @var InseeBanAddress
	 */
	protected InseeBanAddress $_address;
	
	/**
	 * The fitness value of the group.
	 * 
	 * @var float [0,1]
	 */
	protected float $_fitness;
	
	/**
	 * Builds a new InseeBanBayesianGroup with its fitness value.
	 * 
	 * @param InseeBanBayesianGroup $bayesianGroup
	 * @param InseeBanAddress $address
	 * @param float $fitness
	 */
	public function __construct(InseeBanBayesianGroup $bayesianGroup, InseeBanAddress $address, float $fitness)
	{
		$this->_bayesianGroup = $bayesianGroup;
		$this->_address = $address;
		$this->_fitness = \min(1.0, \max(0.0, $fitness));
	}
	
	/**
	 * Gets the bayesian group.
	 * 
	 * @return InseeBanBayesianGroup
	 */
	public function getBayesianGroup() : InseeBanBayesianGroup
	{
		return $this->_bayesianGroup;
	}
	
	/**
	 * Gets the address.
	 * 
	 * @return InseeBanAddress
	 */
	public function getAddress() : InseeBanAddress
	{
		return $this->_address;
	}
	
	/**
	 * Gets the fitness of the value, as probability.
	 * 
	 * @return float
	 */
	public function getFitness() : float
	{
		return $this->_fitness;
	}
	
	/**
	 * Gets the fitness for the whole bayesian chain.
	 * 
	 * @return float
	 */
	public function getTotalFitness() : float
	{
		return $this->getFitness() * $this->getBayesianGroup()->getTotalFitness();
	}
	
}
