<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use ArrayIterator;
use Countable;
use Iterator;
use IteratorAggregate;
use Yii2Module\Yii2InseeBan\Models\InseeBanGroup;

/**
 * InseeBanBayesianGroupCollection class file.
 * 
 * This class represents a collection that handles search results over groups.
 * 
 * @author Anastaszor
 * @implements \IteratorAggregate<integer, InseeBanBayesianGroup>
 */
class InseeBanBayesianGroupCollection implements Countable, IteratorAggregate
{
	
	/**
	 * The default number of records to keep in a single collection for each
	 * surgroup (here the number of groups per postal area).
	 * 
	 * @var integer
	 */
	public const DEFAULT_MAX_RECORDS_PER_AREA = 7;
	
	/**
	 * The max number of records to keep in a single collection per postal area.
	 * 
	 * @var integer
	 */
	protected int $_maxRecordsPerArea = self::DEFAULT_MAX_RECORDS_PER_AREA;
	
	/**
	 * The data from the groups.
	 * 
	 * @var array<integer, InseeBanBayesianGroup>
	 */
	protected array $_data = [];
	
	/**
	 * Builds a new InseeBanBayesianGroupCollection with the given quantity of
	 * records to hold.
	 * 
	 * @param int $maxRecordsPerArea
	 */
	public function __construct(int $maxRecordsPerArea = self::DEFAULT_MAX_RECORDS_PER_AREA)
	{
		$this->_maxRecordsPerArea = $maxRecordsPerArea;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorAggregate::getIterator()
	 */
	public function getIterator() : Iterator
	{
		return new ArrayIterator($this->_data);
	}
	
	/**
	 * Adds a new bayesian group to the collection.
	 * 
	 * @param InseeBanBayesianGroup $group
	 * @return InseeBanBayesianGroupCollection
	 */
	public function add(InseeBanBayesianGroup $group) : InseeBanBayesianGroupCollection
	{
		$countRecordsForArea = 0;
		
		/** @var InseeBanBayesianGroup $record */
		foreach($this->_data as $record)
		{
			$countRecordsForArea += (int) ($record->getBayesianArea()->getArea()->insee_ban_postal_area_id === $group->getBayesianArea()->getArea()->insee_ban_postal_area_id);
		}
		
		if(0 < $this->_maxRecordsPerArea && $countRecordsForArea > $this->_maxRecordsPerArea)
		{
			$rmvVal = $group->getTotalFitness();
			$rmvIdx = null; // null we remove nothing, else remove a record
			
			/** @var InseeBanBayesianGroup $record */
			foreach($this->_data as $k => $record)
			{
				if($record->getBayesianArea()->getArea()->insee_ban_postal_area_id === $group->getBayesianArea()->getArea()->insee_ban_postal_area_id)
				{
					$fitness = $record->getTotalFitness();
					if($fitness < $rmvVal)
					{
						$rmvVal = $fitness;
						$rmvIdx = $k;
					}
				}
			}
			
			if(null === $rmvIdx)
			{
				return $this;
			}
			
			unset($this->_data[$rmvIdx]);
		}
		
		$this->_data[] = $group;
		
		return $this;
	}
	
	/**
	 * Adds a new group and its fitness value to the collection.
	 * 
	 * @param InseeBanBayesianPostalArea $bayesianArea
	 * @param InseeBanGroup $group
	 * @param float $fitness
	 * @return InseeBanBayesianGroupCollection
	 */
	public function addp(InseeBanBayesianPostalArea $bayesianArea, InseeBanGroup $group, float $fitness) : InseeBanBayesianGroupCollection
	{
		return $this->add(new InseeBanBayesianGroup($bayesianArea, $group, $fitness));
	}
	
	/**
	 * Gets whether this list is empty.
	 *
	 * @return boolean
	 */
	public function isEmpty() : bool
	{
		return \count($this->_data) === 0;
	}
	
	/**
	 * Gets the best fit from the collection.
	 * 
	 * @return ?InseeBanBayesianGroup
	 */
	public function getBestFit() : ?InseeBanBayesianGroup
	{
		$bestGroup = null;
		$bestScore = 0;
		
		foreach($this->_data as $group)
		{
			/** @var InseeBanBayesianGroup $group */
			$totalFitness = $group->getTotalFitness();
			if($totalFitness > $bestScore)
			{
				$bestScore = $totalFitness;
				$bestGroup = $group;
			}
		}
		
		return $bestGroup;
	}
	
}
