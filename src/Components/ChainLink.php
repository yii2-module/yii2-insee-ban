<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

/**
 * ChainLink class file.
 * 
 * This object is made to build chains of objects that are hold as encapsulated
 * objects.
 * 
 * @author Anastaszor
 * @template T of object
 */
class ChainLink
{
	
	/**
	 * The encapsulated object.
	 * 
	 * @var T
	 */
	protected object $_object;
	
	/**
	 * The next link.
	 * 
	 * @var ?ChainLink<T>
	 */
	protected ?ChainLink $_next;
	
	/**
	 * Builds a new ChainLink with the given object encapsulated.
	 * 
	 * @param T $object
	 * @param ?ChainLink<T> $next
	 */
	public function __construct($object, ?ChainLink $next = null)
	{
		$this->_object = $object;
		$this->_next = $next;
	}
	
	/**
	 * Gets the object this chain link is holding.
	 * 
	 * @return T
	 */
	public function getObject() : object
	{
		return $this->_object;
	}
	
	/**
	 * Gets the next chain link, if exists.
	 * 
	 * @return ?ChainLink<T>
	 */
	public function getNext() : ?ChainLink
	{
		return $this->_next;
	}
	
}
