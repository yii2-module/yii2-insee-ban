<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLine;
use PhpExtended\Slugifier\SlugifierInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Yii2InseeBan\Models\InseeBanAddress;
use Yii2Module\Yii2InseeBan\Models\InseeBanAddressHistory;
use Yii2Module\Yii2InseeBan\Models\InseeBanGroup;
use Yii2Module\Yii2InseeBan\Models\InseeBanGroupHistory;
use Yii2Module\Yii2InseeBan\Models\InseeBanPosition;
use Yii2Module\Yii2InseeBan\Models\InseeBanPositionHistory;
use Yii2Module\Yii2InseeBan\Models\InseeBanPostalArea;
use Yii2Module\Yii2InseeBan\Models\InseeBanPostalAreaHistory;

/**
 * InseeBanBulkGatherer class file.
 * 
 * This class gathers all the data from the outside files to rebuild the data
 * history.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeBanBulkGatherer
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The endpoint.
	 * 
	 * @var ApiFrInseeBanEndpointInterface
	 */
	protected ApiFrInseeBanEndpointInterface $_endpoint;
	
	/**
	 * The slugifier.
	 * 
	 * @var SlugifierInterface
	 */
	protected SlugifierInterface $_slugifier;
	
	/**
	 * The splitter.
	 * 
	 * @var InseeBanSplitter
	 */
	protected InseeBanSplitter $_splitter;
	
	/**
	 * The id of the target departement.
	 * 
	 * @var string
	 */
	protected string $_departementId;
	
	/**
	 * The known postal codes.
	 * 
	 * @var InseeBanKnownPostalCodes
	 */
	protected InseeBanKnownPostalCodes $_knownPostalCodes;
	
	/**
	 * Gets the known ign group ids.
	 * [string:ban_id => string:ign_id].
	 *
	 * @var array<string, string>
	 */
	protected array $_knownIgnGroupIds = [];
	
	/**
	 * Gets the known ign address ids.
	 * [string:ban_id => string:ign_id].
	 *
	 * @var array<string, string>
	 */
	protected array $_knownIgnAddressIds = [];
	
	/**
	 * The postal areas.
	 * 
	 * @var array<string, ChainLink<InseeBanPostalArea>>
	 */
	protected array $_postalAreas = [];
	
	/**
	 * The groups.
	 * 
	 * @var array<string, ChainLink<InseeBanGroup>>
	 */
	protected array $_groups = [];
	
	/**
	 * The addresses.
	 * 
	 * @var array<string, ChainLink<InseeBanAddress>>
	 */
	protected array $_adresses = [];
	
	/**
	 * The positions.
	 * 
	 * @var array<string, ChainLink<InseeBanPosition>>
	 */
	protected array $_positions = [];
	
	/**
	 * Builds a new InseeBanBulkGatherer with the given components.
	 * 
	 * @param LoggerInterface $logger
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param SlugifierInterface $slugifier
	 * @param InseeBanSplitter $splitter
	 * @param string $departementId
	 */
	public function __construct(
		LoggerInterface $logger,
		ApiFrInseeBanEndpointInterface $endpoint,
		SlugifierInterface $slugifier,
		InseeBanSplitter $splitter,
		string $departementId
	) {
		$this->_logger = $logger;
		$this->_endpoint = $endpoint;
		$this->_slugifier = $slugifier;
		$this->_splitter = $splitter;
		$this->_departementId = $departementId;
		$this->_knownPostalCodes = new InseeBanKnownPostalCodes();
	}
	
	/**
	 * Absorbs the given data for the given date.
	 * 
	 * @param DateTimeInterface $date
	 * @return integer the number of records gathered
	 * @throws RuntimeException
	 */
	public function gatherForDate(DateTimeInterface $date) : int
	{
		$this->_logger->info('Processing Ban Lines for departement {did} and date {dtv}', [
			'did' => $this->_departementId,
			'dtv' => $date->format('Y-m-d'),
		]);
		
		$count = 0;
		
		/** @var \PhpExtended\ApiFrInseeBan\ApiFrInseeBanIgnGroup $ignGroup */
		foreach($this->_endpoint->getBanIgnGroupsIterator($date, $this->_departementId) as $qty => $ignGroup)
		{
			if(0 === (((int) $qty + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Ign Group Lines...', ['k' => (int) $qty + 1]);
			}
			
			$this->_knownIgnGroupIds[(string) $ignGroup->getIdBanGroup()] = (string) $ignGroup->getIdIgnGroup();
			$count++;
		}
		
		/** @var \PhpExtended\ApiFrInseeBan\ApiFrInseeBanIgnAddress $ignAddress */
		foreach($this->_endpoint->getBanIgnAddressesIterator($date, $this->_departementId) as $qty => $ignAddress)
		{
			if(0 === (((int) $qty + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Ban Ign Address...', ['k' => (int) $qty + 1]);
			}
			
			$this->_knownIgnAddressIds[(string) $ignAddress->getIdBanAdresse()] = (string) $ignAddress->getIdIgnAdresse();
			$count++;
		}
		
		/** @var ApiFrInseeBanLine $banLine */
		foreach($this->_endpoint->getBanLinesIterator($date, $this->_departementId) as $qty => $banLine)
		{
			if(0 === (((int) $qty + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Ban Lines...', ['k' => (int) $qty + 1]);
			}
			
			$count += $this->absorbBanLine($banLine);
		}
		
		return $count;
	}
	
	/**
	 * Absorbs the given ban line with all history for all levels.
	 * 
	 * @param ApiFrInseeBanLine $banLine
	 * @return integer the number of lines absorbed
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function absorbBanLine(ApiFrInseeBanLine $banLine) : int
	{
		$saved = 0;
		$codePostal = $this->_knownPostalCodes->getPostalCode($banLine);
		$code = ((string) $banLine->getCodeInsee()).$codePostal;
		$lid = $this->b64usFromHex($code);
		$gid = $this->b64usFromHex(\mb_substr((string) $banLine->getIdBanGroup().'0'.$codePostal, 10)); // ban-group-
		$aid = $this->b64usFromHex(\mb_substr((string) $banLine->getIdBanAdresse(), 16)); // ban-housenumber-
		$pid = $this->b64usFromHex(\mb_substr((string) $banLine->getIdBanPosition(), 13)); // ban-position-
		$fantoirId = empty($banLine->idFantoir) ? null : $banLine->getIdFantoir();
		$ignGroupId = $this->_knownIgnGroupIds[(string) $banLine->getIdBanGroup()] ?? null;
		$knownIgnAddressId = $this->_knownIgnAddressIds[(string) $banLine->getIdBanAdresse()] ?? null;
		$ignAddressId = null === $knownIgnAddressId ? null : (int) \ltrim(\mb_substr($knownIgnAddressId, 8), '0'); // ADRNIVX_
		$nomComplementaire = $banLine->getNomComplementaire() === null || $banLine->getNomComplementaire() === '' ? null : $banLine->getNomComplementaire();
		$split = $this->_splitter->split($banLine->getNomVoie());
		$numero = $banLine->getNumero();
		$suffixe = $banLine->getSuffixe() === null || $banLine->getSuffixe() === '' ? null : $this->_slugifier->slugify($banLine->getSuffixe());
		$xCo = (int) ((float) $banLine->getX() * 100.0);
		$yCo = (int) ((float) $banLine->getY() * 100.0);
		$lat = (int) ((float) $banLine->getLat() * 1000000.0);
		$lon = (int) ((float) $banLine->getLon() * 1000000.0);
		
		// {{{ step 1 : gather for postal area 
		/** @var ?ChainLink<InseeBanPostalArea> $chainLink */
		$chainLink = $this->_postalAreas[$lid] ?? null;
		$mustBeSaved = true;
		
		if(null !== $chainLink)
		{
			/** @var InseeBanPostalArea $object */
			$object = $chainLink->getObject();
			
			$mustBeSaved = ($banLine->getCodeInsee() !== ((string) $object->insee_cog_commune_id))
				|| (((string) $object->postal_code) !== $codePostal)
				|| ($banLine->getNomCommune() !== ((string) $object->name));
		}
		
		if($mustBeSaved)
		{
			$postalArea = new InseeBanPostalArea();
			$postalArea->insee_ban_postal_area_id = $lid;
			$postalArea->insee_cog_commune_id = (string) $banLine->getCodeInsee();
			$postalArea->date_last_update = $banLine->getDateDerMajGroup()->format('Y-m-d');
			$postalArea->postal_code = $codePostal;
			$postalArea->name = (string) $banLine->getNomCommune();
			$this->_postalAreas[$lid] = new ChainLink($postalArea, $chainLink);
			$saved++;
		}
		// }}} end step 1
		
		// {{{ step 2 : gather for group
		/** @var ?ChainLink<InseeBanGroup> $chainLink */
		$chainLink = $this->_groups[$gid] ?? null;
		$mustBeSaved = true;
		
		if(null !== $chainLink)
		{
			/** @var InseeBanGroup $object */
			$object = $chainLink->getObject();
			
			$mustBeSaved = (string) $object->insee_ban_postal_area_id !== (string) $lid
				|| (string) $object->fantoir_id !== (string) $fantoirId
				|| (string) $object->ign_group_id !== (string) $ignGroupId
				|| (string) $object->insee_ban_type_voie_id !== (string) $split->getTypeVoie()->getId()
				|| (string) $object->way_name !== (string) $split->getRemaining()
				|| (string) $object->complement_name !== (string) $nomComplementaire;
		}
		
		if($mustBeSaved)
		{
			$group = new InseeBanGroup();
			$group->insee_ban_group_id = $gid;
			$group->insee_ban_postal_area_id = $lid;
			$group->date_last_update = $banLine->getDateDerMajGroup()->format('Y-m-d');
			$group->fantoir_id = $fantoirId;
			$group->ign_group_id = $ignGroupId;
			$group->insee_ban_type_voie_id = (int) $split->getTypeVoie()->getId();
			$group->way_name = $split->getRemaining();
			$group->complement_name = $nomComplementaire;
			$this->_groups[$gid] = new ChainLink($group, $chainLink);
			$saved++;
		}
		// }}}
		
		// {{{ step 3 : gather for addresses
		/** @var ?ChainLink<InseeBanAddress> $chainLink */
		$chainLink = $this->_adresses[$aid] ?? null;
		$mustBeSaved = true;
		
		if(null !== $chainLink)
		{
			/** @var InseeBanAddress $object */
			$object = $chainLink->getObject();
			
			$mustBeSaved = ((string) $object->insee_ban_group_id) !== ((string) $gid)
				|| ((string) $object->ign_address_id) !== ((string) $ignAddressId)
				|| ((string) $this->_slugifier->slugify($object->interop_id)) !== ((string) $this->_slugifier->slugify($banLine->getCleInterop()))
				|| ((string) $object->number) !== ((string) $numero)
				|| ((string) $object->suffixe) !== ((string) $suffixe);
		}
		
		if($mustBeSaved)
		{
			$address = new InseeBanAddress();
			$address->insee_ban_address_id = $aid;
			$address->insee_ban_group_id = $gid;
			$address->date_last_update = $banLine->getDateDerMajHn()->format('Y-m-d');
			$address->ign_address_id = $ignAddressId;
			$address->interop_id = $this->_slugifier->slugify($banLine->getCleInterop());
			$address->number = (int) $numero;
			$address->suffixe = $suffixe;
			$this->_adresses[$aid] = new ChainLink($address, $chainLink);
			$saved++;
		}
		// }}} end step 3
		
		// {{{ step 4 : gather for position
		/** @var ?ChainLink<InseeBanPosition> $chainLink */
		$chainLink = $this->_positions[$pid] ?? null;
		$mustBeSaved = true;
		
		if(null !== $chainLink)
		{
			/** @var InseeBanPosition $object */
			$object = $chainLink->getObject();
			
			$mustBeSaved = (string) $object->insee_ban_address_id !== (string) $aid
				|| (string) $object->insee_ban_localisation_id !== (string) $banLine->getTypLoc()->getId()
				|| (string) $object->insee_ban_source_id !== (string) $banLine->getSource()->getId()
				|| (string) $object->x !== (string) $xCo
				|| (string) $object->y !== (string) $yCo
				|| (string) $object->latitude !== (string) $lat
				|| (string) $object->longitude !== (string) $lon;
		}
		
		if($mustBeSaved)
		{
			$position = new InseeBanPosition();
			$position->insee_ban_position_id = $pid;
			$position->insee_ban_address_id = $aid;
			$position->date_last_update = $banLine->getDateDerMajPos()->format('Y-m-d');
			$position->insee_ban_localisation_id = (int) $banLine->getTypLoc()->getId();
			$position->insee_ban_source_id = (int) $banLine->getSource()->getId();
			$position->x = $xCo;
			$position->y = $yCo;
			$position->latitude = $lat;
			$position->longitude = $lon;
			$this->_positions[$pid] = new ChainLink($position, $chainLink);
			$saved++;
		}
		// }}} end step 4
		
		return $saved;
	}
	
	/**
	 * Gets the base64 urlsafe encoded string of the given hexadecimal string.
	 * The hexadecimal string must have an even number of hexadecimal digits.
	 *
	 * @param string $hexa
	 * @return string
	 */
	public function b64usFromHex(string $hexa)
	{
		return \str_replace(['+', '/'], ['-', '_'], \trim(\base64_encode((string) \hex2bin($hexa)), '='));
	}
	
	/**
	 * Saves all the records to the database.
	 * 
	 * @param null|callable():void $callback
	 * @return integer
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveAll(?callable $callback = null) : int
	{
		$count = 0;
		$count += $this->saveAllPostalAreas($callback);
		$count += $this->saveAllGroups($callback);
		$count += $this->saveAllAddresses($callback);
		$count += $this->saveAllPositions($callback);
		
		return $count;
	}
	
	/**
	 * Saves all the given postal areas.
	 * 
	 * @param null|callable():void $callback
	 * @return integer
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveAllPostalAreas(?callable $callback = null) : int
	{
		$saver = new InseeBanRecordManager(InseeBanPostalArea::class, InseeBanPostalAreaHistory::class, 'date_last_update', 'date_end_validity', [
			'insee_cog_commune_id' => 'insee_cog_commune_id',
			'postal_code' => 'postal_code',
			'name' => 'name',
		]);
		$count = 0;
		$qty = 0;
		
		$this->_logger->info('Beginning Processing of Postal Areas');
		
		/** @var ChainLink<InseeBanPostalArea> $chainPostalArea */
		foreach($this->_postalAreas as $chainPostalArea)
		{
			$qty++;
			if($qty % 10000 === 0)
			{
				$message = 'Processing {k} Postal Areas ...';
				$context = ['k' => $qty];
				
				$this->_logger->info($message, $context);
				if(null !== $callback)
				{
					$callback();
				}
			}
			
			$object = $chainPostalArea->getObject();
			$count += $saver->updateRecord($object);
			
			$next = $chainPostalArea->getNext();
			
			while(null !== $next)
			{
				$count += $saver->updateRecordAsHistory($next->getObject());
				$next = $next->getNext();
			}
		}
		
		$this->_logger->info('End of Processing of Postal Areas');
		
		return $count;
	}
	
	/**
	 * Saves all the given groups.
	 * 
	 * @param null|callable():void $callback
	 * @return integer
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveAllGroups(?callable $callback = null) : int
	{
		$saver = new InseeBanRecordManager(InseeBanGroup::class, InseeBanGroupHistory::class, 'date_last_update', 'date_end_validity', [
			'insee_ban_postal_area_id' => 'insee_ban_postal_area_id',
			'insee_ban_type_voie_id' => 'insee_ban_type_voie_id',
			'fantoir_id' => 'fantoir_id',
			'ign_group_id' => 'ign_group_id',
			'way_name' => 'way_name',
			'complement_name' => 'complement_name',
		]);
		$count = 0;
		$qty = 0;
		
		$this->_logger->info('Beginning Processing of Groups');
		
		/** @var ChainLink<InseeBanGroup> $chainGroup */
		foreach($this->_groups as $chainGroup)
		{
			$qty++;
			if($qty % 10000 === 0)
			{
				$message = 'Processing {k} Groups ...';
				$context = ['k' => $qty];
				
				$this->_logger->info($message, $context);
				if(null !== $callback)
				{
					$callback();
				}
			}
			
			$object = $chainGroup->getObject();
			$count += $saver->updateRecord($object);
			
			$next = $chainGroup->getNext();
			
			while(null !== $next)
			{
				$count += $saver->updateRecordAsHistory($next->getObject());
				$next = $next->getNext();
			}
		}
		
		$this->_logger->info('End of Processing of Groups');
		
		return $count;
	}
	
	/**
	 * Saves all the given addresses.
	 * 
	 * @param null|callable():void $callback
	 * @return integer
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveAllAddresses(?callable $callback = null) : int
	{
		$saver = new InseeBanRecordManager(InseeBanAddress::class, InseeBanAddressHistory::class, 'date_last_update', 'date_end_validity', [
			'insee_ban_group_id' => 'insee_ban_group_id',
			'ign_address_id' => 'ign_address_id',
			'interop_id' => 'interop_id',
			'number' => 'number',
			'suffixe' => 'suffixe',
		]);
		$count = 0;
		$qty = 0;
		
		$this->_logger->info('Beginning Processing of Addresses');
		
		/** @var ChainLink<InseeBanAddress> $chainAddress */
		foreach($this->_adresses as $chainAddress)
		{
			$qty++;
			if($qty % 10000 === 0)
			{
				$message = 'Processing {k} Addresses ...';
				$context = ['k' => $qty];
				
				$this->_logger->info($message, $context);
				if(null !== $callback)
				{
					$callback();
				}
			}
			
			$object = $chainAddress->getObject();
			$count += $saver->updateRecord($object);
			
			$next = $chainAddress->getNext();
			
			while(null !== $next)
			{
				$count += $saver->updateRecordAsHistory($next->getObject());
				$next = $next->getNext();
			}
		}
		
		$this->_logger->info('End of Processing of Addresses');
		
		return $count;
	}
	
	/**
	 * Saves all the given positions.
	 * 
	 * @param null|callable():void $callback
	 * @return integer
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveAllPositions(?callable $callback = null) : int
	{
		$saver = new InseeBanRecordManager(InseeBanPosition::class, InseeBanPositionHistory::class, 'date_last_update', 'date_end_validity', [
			'insee_ban_address_id' => 'insee_ban_address_id',
			'insee_ban_localisation_id' => 'insee_ban_localisation_id',
			'insee_ban_source_id' => 'insee_ban_source_id',
			'x' => 'x',
			'y' => 'y',
			'latitude' => 'latitude',
			'longitude' => 'longitude',
		]);
		$count = 0;
		$qty = 0;
		
		$this->_logger->info('Beginning Processing of Positions');
		
		foreach($this->_positions as $chainPosition)
		{
			$qty++;
			if($qty % 10000 === 0)
			{
				$message = 'Processing {k} Positions ...';
				$context = ['k' => $qty];
				
				$this->_logger->info($message, $context);
				if(null !== $callback)
				{
					$callback();
				}
			}
			
			$object = $chainPosition->getObject();
			$count += $saver->updateRecord($object);
			
			$next = $chainPosition->getNext();
			
			while(null !== $next)
			{
				$count += $saver->updateRecordAsHistory($next->getObject());
				$next = $next->getNext();
			}
		}
		
		$this->_logger->info('End of Processing of Positions');
		
		return $count;
	}
	
}
