<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use Yii2Module\Yii2InseeBan\Models\InseeBanGroup;

/**
 * InseeBanBayesianGroup class file.
 * 
 * This class represents a search result over a group with its given fitness.
 * 
 * @author Anastaszor
 */
class InseeBanBayesianGroup
{
	
	/**
	 * The area this group is in.
	 * 
	 * @var InseeBanBayesianPostalArea
	 */
	protected InseeBanBayesianPostalArea $_bayesianArea;
	
	/**
	 * The targeted group.
	 * 
	 * @var InseeBanGroup
	 */
	protected InseeBanGroup $_group;
	
	/**
	 * The fitness value of the group.
	 * 
	 * @var float [0,1]
	 */
	protected float $_fitness;
	
	/**
	 * Builds a new InseeBanBayesianPostalArea with its fitness value.
	 * 
	 * @param InseeBanBayesianPostalArea $bayesianArea
	 * @param InseeBanGroup $group
	 * @param float $fitness
	 */
	public function __construct(InseeBanBayesianPostalArea $bayesianArea, InseeBanGroup $group, float $fitness)
	{
		$this->_bayesianArea = $bayesianArea;
		$this->_group = $group;
		$this->_fitness = \min(1.0, \max(0.0, $fitness));
	}
	
	/**
	 * Gets the bayesian postal area.
	 * 
	 * @return InseeBanBayesianPostalArea
	 */
	public function getBayesianArea() : InseeBanBayesianPostalArea
	{
		return $this->_bayesianArea;
	}
	
	/**
	 * Gets the group.
	 * 
	 * @return InseeBanGroup
	 */
	public function getGroup() : InseeBanGroup
	{
		return $this->_group;
	}
	
	/**
	 * Gets the fitness of the value, as probability.
	 * 
	 * @return float
	 */
	public function getFitness() : float
	{
		return $this->_fitness;
	}
	
	/**
	 * Gets the fitness for the whole bayesian chain.
	 * 
	 * @return float
	 */
	public function getTotalFitness() : float
	{
		return $this->getFitness() * $this->getBayesianArea()->getTotalFitness();
	}
	
}
