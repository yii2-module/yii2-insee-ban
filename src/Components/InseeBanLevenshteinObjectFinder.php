<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use InvalidArgumentException;
use PhpExtended\Slugifier\SlugifierFactory;
use PhpExtended\Slugifier\SlugifierInterface;
use PhpExtended\Slugifier\SlugifierOptions;
use RuntimeException;
use Yii2Module\Yii2InseeBan\Models\InseeBanAddress;
use Yii2Module\Yii2InseeBan\Models\InseeBanGroup;
use Yii2Module\Yii2InseeBan\Models\InseeBanPostalArea;

/**
 * InseeBanLevenshteinObjectFinder class file.
 * 
 * This class tries to find BAN objects from data using levenshtein distance
 * as discriminant.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class InseeBanLevenshteinObjectFinder
{
	
	/**
	 * The slugifier, for the searches.
	 *
	 * @var ?SlugifierInterface
	 */
	protected ?SlugifierInterface $_slugifier = null;
	
	/**
	 * Gets the slugifier.
	 *
	 * @return SlugifierInterface
	 */
	public function getSlugifier() : SlugifierInterface
	{
		if(null === $this->_slugifier)
		{
			$factory = new SlugifierFactory();
			$options = new SlugifierOptions();
			$options->setSeparator('-');
			$this->_slugifier = $factory->createSlugifier($options);
		}
		
		return $this->_slugifier;
	}
	
	/**
	 * Tries to find the postal area that correspond to the given code postal
	 * and code commune.
	 *
	 * If the code commune is not given, then a levenshtein search will be
	 * performed on the libelle commune after deducing the departement of
	 * research (assuming there are no two communes with the same name in the
	 * same department (in the given levenshtein tolerance), which is false,
	 * check wikipedia !) from the postal code.
	 *
	 * If both the code commune and the libelle commune are not given, then the
	 * postal code only will be used to check for an area. An exception is
	 * thrown if there are multiple postal areas that qualify for only this
	 * code postal.
	 *
	 * If the code postal is not given but the code commune is, then if there
	 * is only one area, it will be returned, otherwise an exception is thrown
	 * if there are multiple postal areas that qualify for this commune.
	 *
	 * @param ?string $codePostal
	 * @param ?string $libelleCommune
	 * @param ?string $codeCommune
	 * @param integer $levenshteinTolerance
	 * @return InseeBanPostalArea
	 * @throws InvalidArgumentException if the args do not describe an area
	 * @throws RuntimeException if the postal area cannot be found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function getBanPostalArea(
		?string $codePostal,
		?string $libelleCommune,
		?string $codeCommune,
		int $levenshteinTolerance = 2
	) : InseeBanPostalArea {
		
		if((null === $codePostal || '' === $codePostal) && (null === $libelleCommune || '' === $libelleCommune) && (null === $codeCommune || '' === $codeCommune))
		{
			$message = 'Impossible to find a postal area : codePostal, libelleCommune and codeCommune are all empty.';
			
			throw new InvalidArgumentException($message);
		}
		
		$levenshteinTolerance = \max(0, $levenshteinTolerance); // >= 0
		
		if(null !== $codeCommune && '' !== $codeCommune && null !== $codePostal && '' !== $codePostal)
		{
			$banPostalArea = InseeBanPostalArea::findOne([
				'insee_cog_commune_id' => $codeCommune,
				'postal_code' => $codePostal,
			]);
			if(null !== $banPostalArea)
			{
				return $banPostalArea;
			}
		}
		
		if(null !== $codeCommune && '' !== $codeCommune)
		{
			$postalAreas = InseeBanPostalArea::findAll([
				'insee_cog_commune_id' => $codeCommune,
			]);
			// if the result is only one, return it regardless of the ldistance
			if(\count($postalAreas) === 1 && isset($postalAreas[0]))
			{
				return $postalAreas[0];
			}
		}
		
		if(null !== $codePostal && '' !== $codePostal)
		{
			$postalAreas = InseeBanPostalArea::findAll([
				'postal_code' => $codePostal,
			]);
			
			// if the result is only one, return it regardless of the ldistance
			if(\count($postalAreas) === 1 && isset($postalAreas[0]))
			{
				return $postalAreas[0];
			}
			
			if(null !== $libelleCommune && '' !== $libelleCommune)
			{
				$libelleCommune = (string) \mb_strtolower($libelleCommune);
				
				$bestBanArea = null;
				$bestLevenshtein = \PHP_INT_MAX;
				$slugCommune = $this->getSlugifier()->slugify($libelleCommune);
				
				// represents the postal areas in which asked name are included
				/** @var array<integer, InseeBanPostalArea> $includes */
				$includes = [];
				
				foreach($postalAreas as $banArea)
				{
					$slugBanArea = $this->getSlugifier()->slugify($banArea->name);
					$levenshtein = \str_levenshtein($slugCommune, $slugBanArea);
					if(\mb_strpos($slugBanArea, $slugCommune) !== false)
					{
						$includes[] = $banArea;
					}
					
					if(0 <= $levenshtein && $levenshtein < $bestLevenshtein)
					{
						$bestLevenshtein = $levenshtein;
						$bestBanArea = $banArea;
					}
				}
				
				if(null !== $bestBanArea && $bestLevenshtein <= $levenshteinTolerance)
				{
					return $bestBanArea;
				}
				
				if(\count($includes) === 1 && isset($includes[0]))
				{
					return $includes[0];
				}
			}
		}
		
		$message = 'Impossible to find a postal area with given codePostal ({cp}), libelleCommune ({libelle}) and codeCommune ({cc}) and levenshtein tolerance {k}';
		$context = [
			'{cp}' => $codePostal ?? 'null',
			'{libelle}' => $libelleCommune ?? 'null',
			'{cc}' => $codeCommune ?? 'null',
			'{k}' => $levenshteinTolerance,
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * Tries to find a group based on the given postal area with the closest
	 * name and complement (if any), within the levenshtein tolerance.
	 *
	 * @param ?string $libelleVoie
	 * @param ?string $complementAdresse
	 * @param InseeBanPostalArea $postalArea
	 * @param integer $levenshteinTolerance
	 * @return InseeBanGroup
	 * @throws InvalidArgumentException if the args do not describe an area
	 * @throws RuntimeException if the postal area cannot be found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function getBanGroupFromArea(
		?string $libelleVoie,
		?string $complementAdresse,
		InseeBanPostalArea $postalArea,
		int $levenshteinTolerance = 2
	) : InseeBanGroup {
		
		if((null === $libelleVoie || '' === $libelleVoie) && (null === $complementAdresse || '' === $complementAdresse))
		{
			$message = 'Impossible to find a group : libelleVoie and complementAdresse are all empty.';
			
			throw new InvalidArgumentException($message);
		}
		
		$libelleVoie = null === $libelleVoie ? null : (string) \mb_strtolower($libelleVoie);
		$complementAdresse = null === $complementAdresse ? null : (string) \mb_strtolower($complementAdresse);
		
		// "CCI PN Délégation Orne" (12 Place du Palais) => "12 Place du Palais"
		if(null !== $libelleVoie && '' !== $libelleVoie && null !== $complementAdresse && '' !== $complementAdresse && \mb_strlen($libelleVoie) > 4)
		{
			if(\mb_substr($libelleVoie, 0, 4) === 'cci ')
			{
				$libelleVoie = $complementAdresse;
				$complementAdresse = null;
			}
		}
		
		$standardAddressParts = [];
		if(null !== $libelleVoie)
		{
			// "RN 113" => "route nationale 113"
			if(\mb_strlen($libelleVoie) > 3 && \mb_substr($libelleVoie, 0, 3) === 'rn ')
			{
				$libelleVoie = 'route nationale '.((string) \mb_substr($libelleVoie, 3));
			}
			
			$standardAddressParts[] = $libelleVoie;
		}
		if(null !== $complementAdresse)
		{
			$standardAddressParts[] = $complementAdresse;
		}
		$standardAddress = \implode(' ', $standardAddressParts);
		
		$bestBanGroup = null;
		$bestLevenshtein = \PHP_INT_MAX;
		$slugAddress = $this->getSlugifier()->slugify($standardAddress);
		
		foreach($postalArea->inseeBanGroups as $banGroup)
		{
			$banGroupAddressParts = [];
			if(!empty($banGroup->way_name))
			{
				$wayName = $banGroup->way_name;
				if(null !== $banGroup->inseeBanTypeVoie)
				{
					$wayName = $banGroup->inseeBanTypeVoie->description.' '.$wayName;
				}
				$banGroupAddressParts[] = \mb_strtolower($wayName);
			}
			if(null !== $banGroup->complement_name && '' !== $banGroup->complement_name)
			{
				$banGroupAddressParts[] = \mb_strtolower($banGroup->complement_name);
			}
			
			$banGroupAddress = \implode(' ', $banGroupAddressParts);
			$slugGroup = $this->getSlugifier()->slugify($banGroupAddress);
			
			$levenshtein = \str_levenshtein($slugAddress, $slugGroup);
			if(0 <= $levenshtein && $levenshtein < $bestLevenshtein)
			{
				$bestLevenshtein = $levenshtein;
				$bestBanGroup = $banGroup;
			}
		}
		
		if(null !== $bestBanGroup && $bestLevenshtein <= $levenshteinTolerance)
		{
			return $bestBanGroup;
		}
		
		$message = 'Failed to find any ban group for postal area {id} {com} {postal} "{libvoie}" ({complement}) within levenshtein tolerance of {k} ({found})';
		$found = 'not found';
		if(null !== $bestBanGroup)
		{
			$found = 'best found at '.((string) $bestLevenshtein).' with';
			$found .= ' '.$bestBanGroup->insee_ban_group_id;
			$found .= ' '.$bestBanGroup->inseeBanTypeVoie->description.' '.$bestBanGroup->way_name;
			if(null !== $bestBanGroup->complement_name && '' !== $bestBanGroup->complement_name)
			{
				$found .= ' '.$bestBanGroup->complement_name;
			}
		}
		
		$context = [
			'{id}' => $postalArea->insee_ban_postal_area_id,
			'{com}' => $postalArea->insee_cog_commune_id,
			'{postal}' => $postalArea->postal_code,
			'{libvoie}' => $libelleVoie ?? 'null',
			'{complement}' => $complementAdresse ?? 'null',
			'{k}' => $levenshteinTolerance,
			'{found}' => $found,
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * Tries to find a group based on the given postal area data with the
	 * closest name and complement (if any), within the levenshtein tolerance.
	 *
	 * @param ?string $libelleVoie
	 * @param ?string $complementAdresse
	 * @param ?string $codePostal
	 * @param ?string $libelleCommune
	 * @param ?string $codeCommune
	 * @param integer $levenshteinTolerance
	 * @return InseeBanGroup
	 * @throws InvalidArgumentException if the args do not describe a group
	 * @throws RuntimeException if the group cannot be found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function getBanGroup(
		?string $libelleVoie,
		?string $complementAdresse,
		?string $codePostal,
		?string $libelleCommune,
		?string $codeCommune,
		int $levenshteinTolerance = 2
	) : InseeBanGroup {
		try
		{
			$postalArea = $this->getBanPostalArea($codePostal, $libelleCommune, $codeCommune, $levenshteinTolerance);
			
			return $this->getBanGroupFromArea($libelleVoie, $complementAdresse, $postalArea, $levenshteinTolerance);
		}
		catch(InvalidArgumentException $exc)
		{
			$message = 'Failed to find ban group from voie : {voie}, complement : {complement}, code postal : {codepostal}, commune : {commune}, code commune : {codecommune}, levenshtein {l}';
			$context = [
				'{voie}' => null === $libelleVoie ? 'null' : '"'.$libelleVoie.'"',
				'{complement}' => null === $complementAdresse ? 'null' : '"'.$complementAdresse.'"',
				'{codepostal}' => null === $codePostal ? 'null' : '"'.$codePostal.'"',
				'{commune}' => null === $libelleCommune ? 'null' : '"'.$libelleCommune.'"',
				'{codecommune}' => null === $codeCommune ? 'null' : '"'.$codeCommune.'"',
				'{l}' => (string) $levenshteinTolerance,
			];
			
			throw new InvalidArgumentException(\strtr($message, $context), -1, $exc);
		}
		catch(RuntimeException $exc)
		{
			$message = 'Failed to find ban group from voie : {voie}, complement : {complement}, code postal : {codepostal}, commune : {commune}, code commune : {codecommune}, levenshtein {l}';
			$context = [
				'{voie}' => null === $libelleVoie ? 'null' : '"'.$libelleVoie.'"',
				'{complement}' => null === $complementAdresse ? 'null' : '"'.$complementAdresse.'"',
				'{codepostal}' => null === $codePostal ? 'null' : '"'.$codePostal.'"',
				'{commune}' => null === $libelleCommune ? 'null' : '"'.$libelleCommune.'"',
				'{codecommune}' => null === $codeCommune ? 'null' : '"'.$codeCommune.'"',
				'{l}' => (string) $levenshteinTolerance,
			];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * Tries to find an address based on its number and repetition index within
	 * the given group.
	 *
	 * @param ?integer $numeroVoie
	 * @param ?string $indiceRepetition
	 * @param InseeBanGroup $banGroup
	 * @return InseeBanAddress
	 * @throws InvalidArgumentException if the args do not describe an area
	 * @throws RuntimeException if the postal area cannot be found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getBanAddressFromGroup(
		?int $numeroVoie,
		?string $indiceRepetition,
		InseeBanGroup $banGroup
	) : InseeBanAddress {
		
		if(null !== $numeroVoie)
		{
			$searchQuery = [
				'insee_ban_group_id' => $banGroup->insee_ban_group_id,
				'number' => $numeroVoie,
			];
			if(null !== $indiceRepetition && '' !== $indiceRepetition)
			{
				$searchQuery['suffixe'] = $indiceRepetition;
			}
			
			$banAddress = InseeBanAddress::findOne($searchQuery);
			if(null !== $banAddress)
			{
				return $banAddress;
			}
			
			// sometimes the dgfip puts adds X000 to unconfirmed numbers
			for($i = 5000; 9000 >= $i; $i += 1000)
			{
				$newNumero = $i + $numeroVoie;
				
				$searchQuery = [
					'insee_ban_group_id' => $banGroup->insee_ban_group_id,
					'number' => $newNumero,
				];
				if(null !== $indiceRepetition && '' !== $indiceRepetition)
				{
					$searchQuery['suffixe'] = $indiceRepetition;
				}
				
				$banAddress = InseeBanAddress::findOne($searchQuery);
				if(null !== $banAddress)
				{
					return $banAddress;
				}
			}
		}
		
		// last try, if there is no ambiguity, go for it
		$addresses = $banGroup->inseeBanAddresses;
		if(\count($addresses) === 1)
		{
			return \reset($addresses);
		}
		
		$message = 'Failed to find any ban address for postal area {pid} {com} {postal}, way {wid} {wty} "{name}" and nb {no}{rep}';
		$context = [
			'{pid}' => $banGroup->inseeBanPostalArea->insee_ban_postal_area_id,
			'{com}' => $banGroup->inseeBanPostalArea->insee_cog_commune_id,
			'{postal}' => $banGroup->inseeBanPostalArea->postal_code,
			'{wid}' => $banGroup->insee_ban_group_id,
			'{wty}' => $banGroup->inseeBanTypeVoie->description,
			'{name}' => $banGroup->way_name,
			'{no}' => null === $numeroVoie ? '(null)' : $numeroVoie,
			'{rep}' => null === $indiceRepetition || '' === $indiceRepetition ? '' : $indiceRepetition,
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * Tries to find an address based on its number and its repetition index,
	 * and all the other data that are used to find a suitable ban group.
	 *
	 * @param ?integer $numeroVoie
	 * @param ?string $indiceRepetition
	 * @param ?string $libelleVoie
	 * @param ?string $complementAdresse
	 * @param ?string $codePostal
	 * @param ?string $libelleCommune
	 * @param ?string $codeCommune
	 * @param integer $levenshteinTolerance
	 * @return InseeBanAddress
	 * @throws InvalidArgumentException if the args do not describe an address
	 * @throws RuntimeException if the address cannot be found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getBanAddress(
		?int $numeroVoie,
		?string $indiceRepetition,
		?string $libelleVoie,
		?string $complementAdresse,
		?string $codePostal,
		?string $libelleCommune,
		?string $codeCommune,
		int $levenshteinTolerance = 2
	) : InseeBanAddress {
		try
		{
			$banGroup = $this->getBanGroup($libelleVoie, $complementAdresse, $codePostal, $libelleCommune, $codeCommune, $levenshteinTolerance);
			
			return $this->getBanAddressFromGroup($numeroVoie, $indiceRepetition, $banGroup);
		}
		catch(InvalidArgumentException $exc)
		{
			$message = 'Failed to find ban group from no : {no}, rep : {rep}, voie : {voie}, complement : {complement}, code postal : {codepostal}, commune : {commune}, code commune : {codecommune}, levenshtein {l}';
			$context = [
				'{no}' => null === $numeroVoie ? 'null' : '"'.((string) $numeroVoie).'"',
				'{rep}' => null === $indiceRepetition ? 'null' : '"'.$indiceRepetition.'"',
				'{voie}' => null === $libelleVoie ? 'null' : '"'.$libelleVoie.'"',
				'{complement}' => null === $complementAdresse ? 'null' : '"'.$complementAdresse.'"',
				'{codepostal}' => null === $codePostal ? 'null' : '"'.$codePostal.'"',
				'{commune}' => null === $libelleCommune ? 'null' : '"'.$libelleCommune.'"',
				'{codecommune}' => null === $codeCommune ? 'null' : '"'.$codeCommune.'"',
				'{l}' => (string) $levenshteinTolerance,
			];
			
			throw new InvalidArgumentException(\strtr($message, $context), -1, $exc);
		}
		catch(RuntimeException $exc)
		{
			$message = 'Failed to find ban group from no : {no}, rep : {rep}, voie : {voie}, complement : {complement}, code postal : {codepostal}, commune : {commune}, code commune : {codecommune}, levenshtein {l}';
			$context = [
				'{no}' => null === $numeroVoie ? 'null' : '"'.((string) $numeroVoie).'"',
				'{rep}' => null === $indiceRepetition ? 'null' : '"'.$indiceRepetition.'"',
				'{voie}' => null === $libelleVoie ? 'null' : '"'.$libelleVoie.'"',
				'{complement}' => null === $complementAdresse ? 'null' : '"'.$complementAdresse.'"',
				'{codepostal}' => null === $codePostal ? 'null' : '"'.$codePostal.'"',
				'{commune}' => null === $libelleCommune ? 'null' : '"'.$libelleCommune.'"',
				'{codecommune}' => null === $codeCommune ? 'null' : '"'.$codeCommune.'"',
				'{l}' => (string) $levenshteinTolerance,
			];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
	}
	
}
