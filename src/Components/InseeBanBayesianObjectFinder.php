<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use InvalidArgumentException;
use PhpExtended\Slugifier\SlugifierFactory;
use PhpExtended\Slugifier\SlugifierInterface;
use PhpExtended\Slugifier\SlugifierOptions;
use RuntimeException;
use Yii2Module\Yii2InseeBan\Models\InseeBanPostalArea;
use Yii2Module\Yii2InseeCog\InseeCogModule;

/**
 * InseeBanBayesianObjectFinder class file.
 * 
 * This class tries to find BAN objects from data using bayesian probabilities
 * to estimate matching between data as discriminant.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class InseeBanBayesianObjectFinder
{
	
	/**
	 * The slugifier, for the searches.
	 *
	 * @var ?SlugifierInterface
	 */
	protected ?SlugifierInterface $_slugifier = null;
	
	/**
	 * Gets the slugifier.
	 *
	 * @return SlugifierInterface
	 */
	public function getSlugifier() : SlugifierInterface
	{
		if(null === $this->_slugifier)
		{
			$factory = new SlugifierFactory();
			$options = new SlugifierOptions();
			$options->setSeparator('-');
			$this->_slugifier = $factory->createSlugifier($options);
		}
		
		return $this->_slugifier;
	}
	
	/**
	 * Tries to find the postal area that matches the given code postal, code
	 * commune and libelle for that commune.
	 * 
	 * If the code postal is not given, then all records that matches the code
	 * commune if given will be returned. If none, then all records that matches
	 * the libelle commune will be returned.
	 * 
	 * If the libelle commune is not given, then all records that matches the
	 * code commune if given will be returned. If none, then all records that
	 * matches the code postal will be returned. If none, then the code postal
	 * will be truncated to 4, and then to 3 characters to simulate fuzzy
	 * search queries.
	 * 
	 * If the code commune is not given, then all records that matches the code
	 * postal, provided it is truncated to 4 or 3 characters if needed, will be
	 * returned. If none, then all records that matches the libelle commune
	 * will be returned.
	 * 
	 * If two of the parameters are missing, the searches will only be conducted
	 * on the last non missing parameter. The search results will be more spread
	 * out however, as a result.
	 * 
	 * The score is calculated with levenshtein distance regarding the query
	 * elements if present, and the distance to the individual elements that
	 * are provided into a postal area objects.
	 * 
	 * @param ?string $codePostal
	 * @param ?string $libelleCommune
	 * @param ?string $codeCommune
	 * @return InseeBanBayesianPostalAreaCollection
	 * @throws InvalidArgumentException if all the arguments are empty
	 * @throws RuntimeException if no search result can be found
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function getBanPostalArea(
		?string $codePostal,
		?string $libelleCommune,
		?string $codeCommune
	) : InseeBanBayesianPostalAreaCollection {
		
		if((null === $codePostal || '' === $codePostal) && (null === $libelleCommune || '' === $libelleCommune) && (null === $codeCommune || '' === $codeCommune))
		{
			$message = 'Impossible to find a postal area with empty code postal, code commune and libelle commune';
			
			throw new InvalidArgumentException($message);
		}
		
		if(null !== $codePostal && '' !== $codePostal && !\preg_match('#^\\d[\\dAB]\\d{1,3}$#', $codePostal))
		{
			$message = 'Inacceptable code postal value "{val}", should match ^\\d[\\dAB]\\d{1,3}$';
			$context = ['{val}' => $codePostal];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		$exc = null;
		$collection = new InseeBanBayesianPostalAreaCollection($this->getSlugifier());
		$codPosSlug = $this->getSlugifier()->slugify((string) $codePostal);
		$codComSlug = $this->getSlugifier()->slugify((string) $codeCommune);
		
		if(!empty($codComSlug) && !empty($codPosSlug))
		{
			/** @var array<integer, InseeBanPostalArea> $banPostalAreas */
			$banPostalAreas = InseeBanPostalArea::findAll([
				'insee_cog_commune_id' => $codComSlug,
				'postal_code' => $codPosSlug,
			]);
			$collection->absorbAll($banPostalAreas, $libelleCommune, $codeCommune, $codePostal);
			if(!$collection->isEmpty())
			{
				return $collection;
			}
		}
		
		// here we have not found ban postal areas with exact code commune
		// and exact code postal
		// we try by exact code commune
		if(!empty($codComSlug))
		{
			/** @var array<integer, InseeBanPostalArea> $banPostalAreas */
			$banPostalAreas = InseeBanPostalArea::findAll([
				'insee_cog_commune_id' => $codComSlug,
			]);
			$collection->absorbAll($banPostalAreas, $libelleCommune, $codeCommune, $codePostal);
			if(!$collection->isEmpty())
			{
				return $collection;
			}
		}
		
		// here we have not found ban postal areas with exact code commune
		// we try by exact code postal
		if(!empty($codPosSlug))
		{
			/** @var array<integer, InseeBanPostalArea> $banPostalAreas */
			$banPostalAreas = InseeBanPostalArea::findAll([
				'postal_code' => $codPosSlug,
			]);
			$collection->absorbAll($banPostalAreas, $libelleCommune, $codeCommune, $codePostal);
			if(!$collection->isEmpty())
			{
				return $collection;
			}
		}
		
		// here we have not found any ban postal areas with exact code commune
		// nor with exact code postal
		// we try with loosen postal code
		if(!empty($codPosSlug))
		{
			/** @var array<integer, InseeBanPostalArea> $banPostalAreas */
			$banPostalAreas = InseeBanPostalArea::find()->andWhere(
				'postal_code LIKE :psc',
				[':psc' => \addcslashes((string) \mb_substr($codPosSlug, 0, 4), '%_').'%'],
			)->all();
			$collection->absorbAll($banPostalAreas, $libelleCommune, $codeCommune, $codePostal);
			if(!$collection->isEmpty())
			{
				return $collection;
			}
		}
		
		// try with even more loosen postal code
		if(!empty($codPosSlug))
		{
			/** @var array<integer, InseeBanPostalArea> $banPostalAreas */
			$banPostalAreas = InseeBanPostalArea::find()->andWhere(
				'postal_code LIKE :psc',
				[':psc' => \addcslashes((string) \mb_substr($codPosSlug, 0, 3), '%_').'%'],
			)->all();
			$collection->absorbAll($banPostalAreas, $libelleCommune, $codeCommune, $codePostal);
			if(!$collection->isEmpty())
			{
				return $collection;
			}
		}
		
		// last try with full search (but we are not elasticsearch, alas)
		if(null !== $libelleCommune && '' !== $libelleCommune)
		{
			/** @var array<integer, InseeBanPostalArea> $banPostalAreas */
			$banPostalAreas = InseeBanPostalArea::find()->where([
				'like', 'name', $libelleCommune,
			])->all();
			
			$collection->absorbAll($banPostalAreas, $libelleCommune, $codeCommune, $codePostal);
			if(!$collection->isEmpty())
			{
				return $collection;
			}
			
			try
			{
				$deptCode = null === $codePostal || '' === $codePostal ? (null === $codeCommune || '' === $codeCommune ? '' : (string) \mb_substr($codeCommune, 0, 2)) : (string) \mb_substr($codePostal, 0, 2);
				$cogCommune = InseeCogModule::getInstance()->getCogCommune($codeCommune, $libelleCommune, $deptCode, 1 + (int) (((int) \mb_strlen($libelleCommune) / 3)));
				/** @var array<integer, InseeBanPostalArea> $banPostalAreas */
				$banPostalAreas = InseeBanPostalArea::findAll(['insee_cog_commune_id' => $cogCommune->insee_cog_commune_id]);
				$collection->absorbAll($banPostalAreas, $libelleCommune, $codeCommune, $codePostal);
				/** @phpstan-ignore-next-line */
				if(!$collection->isEmpty())
				{
					return $collection;
				}
			}
			catch(RuntimeException $excx)
			{
				$exc = $excx;
			}
		}
		
		// we cant find it
		$message = 'Failed to find a postal area with given code commune "{cc}", code postal "{cp}" and libelle commune "{lc}"';
		$context = [
			'{cc}' => $codeCommune ?? '(null)',
			'{cp}' => $codePostal ?? '(null)',
			'{lc}' => $libelleCommune ?? '{lc}',
		];
		
		throw new RuntimeException(\strtr($message, $context), -1, $exc);
	}
	
	/**
	 * Tries to find a group based on the given libelle voie with the closest
	 * name and complement address if any, with levenshtein distance as
	 * discriminant to score.
	 * 
	 * @param ?string $libelleVoie
	 * @param ?string $complementAdresse
	 * @param ?string $codePostal
	 * @param ?string $libelleCommune
	 * @param ?string $codeCommune
	 * @return InseeBanBayesianGroupCollection
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getBanGroup(
		?string $libelleVoie,
		?string $complementAdresse,
		?string $codePostal,
		?string $libelleCommune,
		?string $codeCommune
	) : InseeBanBayesianGroupCollection {
		
		if((null === $libelleVoie || '' === $libelleVoie) && (null === $complementAdresse || '' === $complementAdresse))
		{
			$message = 'Impossible to find a group with an empty libelle voie and complement adresse';
			
			throw new InvalidArgumentException($message);
		}
		
		$postalAreas = $this->getBanPostalArea($codePostal, $libelleCommune, $codeCommune);
		
		$collection = new InseeBanBayesianGroupCollection();
		$libVoieSlug = $this->getSlugifier()->slugify((string) $libelleVoie);
		$compAdrSlug = $this->getSlugifier()->slugify((string) $complementAdresse);
		
		/** @var InseeBanBayesianPostalArea $postalArea */
		foreach($postalAreas as $postalArea)
		{
			foreach($postalArea->getArea()->inseeBanGroups as $banGroup)
			{
				$wayName = $banGroup->way_name;
				if(null !== $banGroup->inseeBanTypeVoie)
				{
					$wayName = $banGroup->inseeBanTypeVoie->description.' '.$wayName;
				}
				$score1 = null === $libelleVoie || '' === $libelleVoie ? 1.0 : $this->getScore($libVoieSlug, $wayName);
				$score2 = null === $complementAdresse || '' === $complementAdresse ? 1.0 : $this->getScore($compAdrSlug, $banGroup->complement_name);
				$collection->addp($postalArea, $banGroup, $score1 * $score2);
			}
		}
		
		if(!$collection->isEmpty())
		{
			return $collection;
		}
		
		$message = 'Failed to find a ban group with given libelle voie "{lv}", complement address "{cmp}", code commune "{cc}", code postal "{cp}" and libelle commune "{lc}"';
		$context = [
			'{lv}' => $libelleVoie ?? '(null)',
			'{cmp}' => $complementAdresse ?? '(null)',
			'{cc}' => $codeCommune ?? '(null)',
			'{cp}' => $codePostal ?? '(null)',
			'{lc}' => $libelleCommune ?? '{lc}',
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * Tries to find an address based on its number and its repetition index, 
	 * and all the other data that are used to find a suitable ban group.
	 * 
	 * @param integer $numeroVoie
	 * @param string $indiceRepetition
	 * @param string $libelleVoie
	 * @param string $complementAdresse
	 * @param string $codePostal
	 * @param string $libelleCommune
	 * @param string $codeCommune
	 * @return InseeBanBayesianAddressCollection
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getBanAddress(
		?int $numeroVoie,
		?string $indiceRepetition,
		?string $libelleVoie,
		?string $complementAdresse,
		?string $codePostal,
		?string $libelleCommune,
		?string $codeCommune
	) : InseeBanBayesianAddressCollection {
		
		$banGroups = $this->getBanGroup($libelleVoie, $complementAdresse, $codePostal, $libelleCommune, $codeCommune);
		
		$collection = new InseeBanBayesianAddressCollection();
		$idxRepSlug = $this->getSlugifier()->slugify((string) $indiceRepetition);
		
		/** @var InseeBanBayesianGroup $banGroup */
		foreach($banGroups as $banGroup)
		{
			foreach($banGroup->getGroup()->inseeBanAddresses as $banAddress)
			{
				$score1 = null === $numeroVoie ? 1.0 : ((1000.0 - ((float) ($banAddress->number % 1000))) / 1000.0);
				$score2 = null === $indiceRepetition || '' === $indiceRepetition ? 1.0 : $this->getScore($idxRepSlug, $banAddress->suffixe);
				$collection->addp($banGroup, $banAddress, $score1 * $score2);
			}
		}
		
		if(!$collection->isEmpty())
		{
			return $collection;
		}
		
		$message = 'Failed to find a ban address with given numero voie {n}, indice repetition "{ir}", libelle voie "{lv}", complement address "{cmp}", code commune "{cc}", code postal "{cp}" and libelle commune "{lc}"';
		$context = [
			'{n}' => $numeroVoie ?? '(null)',
			'{ir}' => $indiceRepetition ?? '(null)',
			'{lv}' => $libelleVoie ?? '(null)',
			'{cmp}' => $complementAdresse ?? '(null)',
			'{cc}' => $codeCommune ?? '(null)',
			'{cp}' => $codePostal ?? '(null)',
			'{lc}' => $libelleCommune ?? '{lc}',
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * Gets the score from levenshtein distance between two strings.
	 * 1 is the best score, and 0 is the worst score.
	 * 
	 * @param string $reference
	 * @param ?string $challenger
	 * @return float [0,1]
	 */
	public function getScore(string $reference, ?string $challenger) : float
	{
		$challenger = $this->getSlugifier()->slugify((string) $challenger);
		
		$denom = (int) \max(1, (string) \mb_strlen($challenger, '8bit'), (string) \mb_strlen($reference, '8bit'));
		$diff = $denom - \str_levenshtein($reference, $challenger);
		
		return ((float) $diff) / ((float) $denom);
	}
	
}
