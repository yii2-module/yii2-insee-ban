<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Yii2InseeBan\Models\InseeBanMetadata;

/**
 * InseeBanAllUpdater class file.
 * 
 * This class updates all the records
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeBanAllUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The postal area updater.
	 * 
	 * @var InseeBanPostalAreaUpdater
	 */
	protected InseeBanPostalAreaUpdater $_postalAreaUpdater;
	
	/**
	 * The group updater.
	 * 
	 * @var InseeBanGroupUpdater
	 */
	protected InseeBanGroupUpdater $_groupUpdater;
	
	/**
	 * The address updater.
	 * 
	 * @var InseeBanAddressUpdater
	 */
	protected InseeBanAddressUpdater $_addressUpdater;
	
	/**
	 * The position updater.
	 * 
	 * @var InseeBanPositionUpdater
	 */
	protected InseeBanPositionUpdater $_positionUpdater;
	
	/**
	 * The date when this update should stop (next cron will relieve).
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_endDate;
	
	/**
	 * Builds a new All Updater with the updater dependancies.
	 * 
	 * @param LoggerInterface $logger
	 * @param InseeBanPostalAreaUpdater $postalAreaUpdater
	 * @param InseeBanGroupUpdater $groupUpdater
	 * @param InseeBanAddressUpdater $addressUpdater
	 * @param InseeBanPositionUpdater $positionUpdater
	 */
	public function __construct(LoggerInterface $logger, InseeBanPostalAreaUpdater $postalAreaUpdater, InseeBanGroupUpdater $groupUpdater, InseeBanAddressUpdater $addressUpdater, InseeBanPositionUpdater $positionUpdater)
	{
		$this->_logger = $logger;
		$this->_postalAreaUpdater = $postalAreaUpdater;
		$this->_groupUpdater = $groupUpdater;
		$this->_addressUpdater = $addressUpdater;
		$this->_positionUpdater = $positionUpdater;
		/** @psalm-suppress PossiblyFalseArgument */
		$this->_endDate = (new DateTimeImmutable())->add(DateInterval::createFromDateString('+20 hours'));
	}
	
	/**
	 * Updates all the ban records for all departements and all dates.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeBanEndpointInterface $endpoint, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateAllDate($endpoint, $date, $force);
			$now = new DateTimeImmutable();
			if($now->getTimestamp() > $this->_endDate->getTimestamp())
			{
				return $count;
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban records for the given department and all dates.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDepartements(ApiFrInseeBanEndpointInterface $endpoint, string $departementId, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
			$now = new DateTimeImmutable();
			if($now->getTimestamp() > $this->_endDate->getTimestamp())
			{
				return $count;
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban records for the given date and all departements.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDate(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, bool $force = false) : int
	{
		$now = new DateTimeImmutable();
		if($date->format('Y-m-d') === $now->format('Y-m-d'))
		{
			$date = $endpoint->getLatestDate();
		}
		$count = 0;
		
		foreach($endpoint->getDepartementIds($date) as $departementId)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
			$now = new DateTimeImmutable();
			if($now->getTimestamp() > $this->_endDate->getTimestamp())
			{
				return $count;
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban positions for the given date and departement.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function updateDepartement(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, string $departementId, bool $force = false) : int
	{
		$this->_logger->info(
			'Processing All Ban Records from Departement {did} at date {date}',
			['did' => $departementId, 'date' => $date->format('Y-m-d')],
		);
		
		$mdpa = InseeBanMetadata::findOne('insee_ban_postal_area.'.$departementId);
		$mdgr = InseeBanMetadata::findOne('insee_ban_group.'.$departementId);
		$mdad = InseeBanMetadata::findOne('insee_ban_address.'.$departementId);
		$mdpo = InseeBanMetadata::findOne('insee_ban_position.'.$departementId);
		if(!$force && null !== $mdpa && null !== $mdgr && null !== $mdad && null !== $mdpo)
		{
			$mdpad = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdpa->contents);
			$mdgrd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdgr->contents);
			$mdadd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdad->contents);
			$mdpod = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdpo->contents);
			if(false !== $mdpad && $mdpad->getTimestamp() > $date->getTimestamp()
				&& false !== $mdgrd && $mdgrd->getTimestamp() > $date->getTimestamp()
				&& false !== $mdadd && $mdadd->getTimestamp() > $date->getTimestamp()
				&& false !== $mdpod && $mdpod->getTimestamp() > $date->getTimestamp()
			) {
				return 0;
			}
		}
		
		$this->_groupUpdater->collectIgnGroupIds($endpoint, $date, $departementId);
		$this->_addressUpdater->collectIgnAddressIds($endpoint, $date, $departementId);
		
		$mdcid = 'insee_ban_all.'.$departementId.'.'.$date->format('Y-m');
		$mdc = InseeBanMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeBanMetadata();
			$mdc->insee_ban_metadata_id = $mdcid;
			$mdc->contents = '0';
			$mdc->save();
		}
		
		$count = 0;
		$postalCodes = new InseeBanKnownPostalCodes();
		
		foreach($endpoint->getBanLinesIterator($date, $departementId) as $k => $banLine)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Ban Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Ban Lines...', ['k' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$mdc->save();
			}
			
			$count += $this->_postalAreaUpdater->updateBanLine($postalCodes, $banLine);
			$count += $this->_groupUpdater->updateBanLine($postalCodes, $banLine);
			$count += $this->_addressUpdater->updateBanLine($postalCodes, $banLine);
			$count += $this->_positionUpdater->updateBanLine($banLine);
		}
		
		if(null === $mdpa)
		{
			$mdpa = new InseeBanMetadata();
			$mdpa->insee_ban_metadata_id = 'insee_ban_postal_area.'.$departementId;
		}
		$dti = null === $mdpa->contents ? null : DateTimeImmutable::createFromFormat('Y-m-d', $mdpa->contents);
		if(empty($dti) || $dti->getTimestamp() < $date->getTimestamp())
		{
			$mdpa->contents = $date->format('Y-m-d');
			$mdpa->save();
		}
		
		if(null === $mdgr)
		{
			$mdgr = new InseeBanMetadata();
			$mdgr->insee_ban_metadata_id = 'insee_ban_group.'.$departementId;
		}
		$dti = null === $mdgr->contents ? null : DateTimeImmutable::createFromFormat('Y-m-d', $mdgr->contents);
		if(empty($dti) || $dti->getTimestamp() < $date->getTimestamp())
		{
			$mdgr->contents = $date->format('Y-m-d');
			$mdgr->save();
		}
		
		if(null === $mdad)
		{
			$mdad = new InseeBanMetadata();
			$mdad->insee_ban_metadata_id = 'insee_ban_address.'.$departementId;
		}
		$dti = null === $mdad->contents ? null : DateTimeImmutable::createFromFormat('Y-m-d', $mdad->contents);
		if(empty($dti) || $dti->getTimestamp() < $date->getTimestamp())
		{
			$mdad->contents = $date->format('Y-m-d');
			$mdad->save();
		}
		
		if(null === $mdpo)
		{
			$mdpo = new InseeBanMetadata();
			$mdpo->insee_ban_metadata_id = 'insee_ban_position.'.$departementId;
		}
		$dti = null === $mdpo->contents ? null : DateTimeImmutable::createFromFormat('Y-m-d', $mdpo->contents);
		if(empty($dti) || $dti->getTimestamp() < $date->getTimestamp())
		{
			$mdpo->contents = $date->format('Y-m-d');
			$mdpo->save();
		}
		
		return $count;
	}
	
}
