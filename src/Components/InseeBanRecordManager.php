<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use DateTimeImmutable;
use InvalidArgumentException;
use RuntimeException;
use yii\db\ActiveRecord;

/**
 * InseeBanRecordManager class file.
 * 
 * This class is made to save historized records into two tables, one with the
 * "fresh" records, and one with the history of records.
 * 
 * @author Anastaszor
 * @template TARClass of ActiveRecord
 * @template TARHistory of ActiveRecord
 */
class InseeBanRecordManager
{
	
	/**
	 * The name of the active record class.
	 * 
	 * @var class-string<TARClass>
	 */
	protected string $_activeRecordClass;
	
	/**
	 * The name of the active record history class.
	 * 
	 * @var class-string<TARHistory>
	 */
	protected string $_activeRecordHistoryClass;
	
	/**
	 * The name of the date field in the active record class.
	 * 
	 * @var string
	 */
	protected string $_activeRecordDateFieldName;
	
	/**
	 * The name of the date field in the active record history class.
	 * 
	 * @var string
	 */
	protected string $_activeRecordHistoryDateFieldName;
	
	/**
	 * The correspondance of field names between the active record and the
	 * active record history for all field names that are considered data.
	 * 
	 * @var array<string, string>
	 */
	protected array $_activeRecordDataFieldNames = [];
	
	/**
	 * Creates a new InseeBanRecordManager with the given class and history
	 * class, date field names and other data fields.
	 * 
	 * @param class-string<TARClass> $activeRecordClass
	 * @param class-string<TARHistory> $activeRecordHistoryClass
	 * @param string $activeRecordDateFieldName
	 * @param string $activeRecordHistoryDateFieldName
	 * @param array<string, string> $activeRecordDataFieldNames
	 * @throws InvalidArgumentException
	 */
	public function __construct(
		string $activeRecordClass,
		string $activeRecordHistoryClass,
		string $activeRecordDateFieldName,
		string $activeRecordHistoryDateFieldName,
		array $activeRecordDataFieldNames
	) {
		$this->_activeRecordClass = $activeRecordClass;
		$this->_activeRecordHistoryClass = $activeRecordHistoryClass;
		
		/** @var ActiveRecord $activeRecord */ /** @psalm-suppress UnsafeInstantiation */
		$activeRecord = new $activeRecordClass();
		/** @var ActiveRecord $activeRecord */ /** @psalm-suppress UnsafeInstantiation */
		$activeRecordHistory = new $activeRecordHistoryClass();
		
		if(!$activeRecord->hasAttribute($activeRecordDateFieldName))
		{
			$message = 'The expected date field name "{fname}" does not exists in the AR class {arclass}';
			$context = ['{fname}' => $activeRecordDateFieldName, '{arclass}' => $activeRecordClass];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		$this->_activeRecordDateFieldName = $activeRecordDateFieldName;
		
		if(!$activeRecordHistory->hasAttribute($activeRecordHistoryDateFieldName))
		{
			$message = 'The expected date field name "{fname}" does not exists in the AR class {arclass}';
			$context = ['{fname}' => $activeRecordHistoryDateFieldName, '{arclass}' => $activeRecordHistoryClass];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		$this->_activeRecordHistoryDateFieldName = $activeRecordHistoryDateFieldName;
		
		foreach($activeRecordDataFieldNames as $activeRecordFieldName => $activeRecordHistoryFieldName)
		{
			if(!$activeRecord->hasAttribute($activeRecordFieldName))
			{
				$message = 'The expected date field name "{fname}" does not exists in the AR class {arclass}';
				$context = ['{fname}' => $activeRecordFieldName, '{arclass}' => $activeRecordClass];
				
				throw new InvalidArgumentException(\strtr($message, $context));
			}
			
			if(!$activeRecordHistory->hasAttribute($activeRecordHistoryFieldName))
			{
				$message = 'The expected date field name "{fname}" does not exists in the AR class {arclass}';
				$context = ['{fname}' => $activeRecordHistoryFieldName, '{arclass}' => $activeRecordHistoryClass];
				
				throw new InvalidArgumentException(\strtr($message, $context));
			}
		}
		
		$this->_activeRecordDataFieldNames = $activeRecordDataFieldNames;
	}
	
	/**
	 * Clones the given active record.
	 * 
	 * @param TARClass $record
	 * @return TARClass
	 * @throws InvalidArgumentException
	 */
	public function cloneRecord(ActiveRecord $record) : ActiveRecord
	{
		try
		{
			/** @var TARClass $newRecord */ /** @psalm-suppress UnsafeInstantiation */
			$newRecord = new $this->_activeRecordClass();
			$newRecord->setIsNewRecord($record->getIsNewRecord());
			
			foreach($record->getPrimaryKey(true) as $pkKey => $pkVal)
			{
				$newRecord->setAttribute((string) $pkKey, $pkVal);
			}
			
			$newRecord->setAttribute($this->_activeRecordDateFieldName, $record->getAttribute($this->_activeRecordDateFieldName));
			
			foreach(\array_keys($this->_activeRecordDataFieldNames) as $fieldName)
			{
				$newRecord->setAttribute($fieldName, $record->getAttribute($fieldName));
			}
			
			return $newRecord;
		}
		catch(\yii\base\InvalidArgumentException $exc)
		{
			throw new InvalidArgumentException($exc->getMessage(), $exc->getCode(), $exc);
		}
	}
	
	/**
	 * Clones the given active record history.
	 * 
	 * @param TARHistory $record
	 * @return TARHistory
	 * @throws InvalidArgumentException
	 */
	public function cloneRecordHistory(ActiveRecord $record) : ActiveRecord
	{
		try
		{
			/** @var ActiveRecord $newRecord */
			/** @var TARHistory $newRecord */ /** @psalm-suppress UnsafeInstantiation */
			$newRecord = new $this->_activeRecordHistoryClass();
			$newRecord->setIsNewRecord($record->getIsNewRecord());
			
			foreach($record->getPrimaryKey(true) as $pkKey => $pkVal)
			{
				$newRecord->setAttribute((string) $pkKey, $pkVal);
			}
			
			$newRecord->setAttribute($this->_activeRecordHistoryDateFieldName, $record->getAttribute($this->_activeRecordHistoryDateFieldName));
			
			foreach(\array_values($this->_activeRecordDataFieldNames) as $fieldName)
			{
				$newRecord->setAttribute($fieldName, $record->getAttribute($fieldName));
			}
			
			return $newRecord;
		}
		catch(\yii\base\InvalidArgumentException $exc)
		{
			throw new InvalidArgumentException($exc->getMessage(), $exc->getCode(), $exc);
		}
	}
	
	/**
	 * Finds the record from the database with the same primary key as the given
	 * record.
	 * 
	 * @param TARClass $record
	 * @return ?TARClass
	 */
	public function findRecord(ActiveRecord $record) : ?ActiveRecord
	{
		$activeRecordClass = $this->_activeRecordClass;
		
		return $activeRecordClass::findOne((array) $record->getPrimaryKey(true));
	}
	
	/**
	 * Finds the record from the database with the same primary key as the given
	 * record.
	 * 
	 * @param TARHistory $record
	 * @return ?TARHistory
	 */
	public function findRecordHistory(ActiveRecord $record) : ?ActiveRecord
	{
		$activeRecordHistoryClass = $this->_activeRecordHistoryClass;
		
		return $activeRecordHistoryClass::findOne(\array_merge(
			(array) $record->getPrimaryKey(true),
			[$this->_activeRecordHistoryDateFieldName => $record->getAttribute($this->_activeRecordHistoryDateFieldName)],
		));
	}
	
	/**
	 * Saves the given record into the record table if the update is valid, and
	 * into the history table if the update is older.
	 * 
	 * Process :
	 * - if the record with the given id does not exists in the database, then
	 *     it is written as is
	 * - else, if the record and the db are different, then
	 *     - if the date of the db record is in the future, then the given
	 *         record is saved as history
	 *     - if the date of the db record is the same as the given record, the
	 *         db record is updated with the db record
	 *     - if the date of the db record is in the past, then the given record
	 *         is updated, and the old db record is saved as history
	 * 
	 * @param TARClass $record
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException if the record cannot be saved
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function updateRecord(ActiveRecord $record) : int
	{
		try
		{
			/** @var ?ActiveRecord $dbRecord */
			/** @var ?TARClass $dbRecord */
			$dbRecord = $this->findRecord($record);
			if(null === $dbRecord)
			{
				return (int) $this->save($this->cloneRecord($record));
			}
			
			$hasChanged = false;
			
			foreach(\array_keys($this->_activeRecordDataFieldNames) as $fieldName)
			{
				$hasChanged = $hasChanged || ((string) $dbRecord->getAttribute($fieldName) !== (string) $record->getAttribute($fieldName));
			}
			
			if(!$hasChanged)
			{
				return 0;
			}
			
			if((string) $dbRecord->getAttribute($this->_activeRecordDateFieldName) === (string) $record->getAttribute($this->_activeRecordDateFieldName))
			{
				foreach(\array_keys($this->_activeRecordDataFieldNames) as $fieldName)
				{
					$dbRecord->setAttribute($fieldName, $record->getAttribute($fieldName));
				}
				
				return (int) $this->save($dbRecord);
			}
			
			$dbDate = DateTimeImmutable::createFromFormat('Y-m-d', (string) $dbRecord->getAttribute($this->_activeRecordDateFieldName));
			if(false === $dbDate)
			{
				$message = 'Failed to parse date with format Y-m-d on db record {class}::{attr}';
				$context = ['{class}' => $this->_activeRecordClass, '{attr}' => $this->_activeRecordDateFieldName];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$date = DateTimeImmutable::createFromFormat('Y-m-d', (string) $record->getAttribute($this->_activeRecordDateFieldName));
			if(false === $date)
			{
				$message = 'Failed to parse date with format Y-m-d on record {class}::{attr}';
				$context = ['{class}' => $this->_activeRecordClass, '{attr}' => $this->_activeRecordDateFieldName];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			// if date is in future, save record as history
			if($dbDate > $date)
			{
				return $this->updateRecordAsHistory($record);
			}
			
			// if date is in the past, update the record and save old record as history
			
			return (int) $this->save($this->cloneRecord($dbRecord)) + $this->updateRecordAsHistory($dbRecord);
		}
		catch(\yii\base\InvalidArgumentException $exc)
		{
			throw new InvalidArgumentException($exc->getMessage(), $exc->getCode(), $exc);
		}
	}
	
	/**
	 * Saves the given record as history.
	 * 
	 * @param TARClass $record
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateRecordAsHistory(ActiveRecord $record) : int
	{
		try
		{
			/** @var ActiveRecord $history */
			/** @var TARHistory $history */ /** @psalm-suppress UnsafeInstantiation */
			$history = new $this->_activeRecordHistoryClass();
			
			foreach($record->getPrimaryKey(true) as $pkKey => $pkVal)
			{
				$history->setAttribute((string) $pkKey, $pkVal);
			}
			
			$history->setAttribute($this->_activeRecordHistoryDateFieldName, $record->getAttribute($this->_activeRecordDateFieldName));
			
			foreach($this->_activeRecordDataFieldNames as $rFieldName => $hFieldName)
			{
				$history->setAttribute($hFieldName, $record->getAttribute($rFieldName));
			}
			
			return $this->updateRecordHistory($history);
		}
		catch(\yii\base\InvalidArgumentException $exc)
		{
			throw new InvalidArgumentException($exc->getMessage(), $exc->getCode(), $exc);
		}
	}
	
	/**
	 * Saves the given record history.
	 * 
	 * @param TARHistory $record
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException if the record cannot be saved
	 */
	public function updateRecordHistory(ActiveRecord $record) : int
	{
		try
		{
			$dbRecord = $this->findRecordHistory($record);
			/** @var ?TARHistory $dbRecord */
			if(null === $dbRecord)
			{
				return (int) $this->save($this->cloneRecordHistory($record));
			}
			
			$hasChanged = false;
			
			foreach(\array_values($this->_activeRecordDataFieldNames) as $fieldName)
			{
				$hasChanged = $hasChanged || ((string) $dbRecord->getAttribute($fieldName) !== (string) $record->getAttribute($fieldName));
			}
			
			if(!$hasChanged)
			{
				return 0;
			}
			
			foreach(\array_values($this->_activeRecordDataFieldNames) as $fieldName)
			{
				$dbRecord->setAttribute($fieldName, $record->getAttribute($fieldName));
			}
			
			return (int) $this->save($dbRecord);
		}
		catch(\yii\base\InvalidArgumentException $exc)
		{
			throw new InvalidArgumentException($exc->getMessage(), $exc->getCode(), $exc);
		}
	}
	
	/**
	 * Saves the given record or fail trying.
	 *
	 * @param ActiveRecord $record
	 * @return boolean
	 * @throws RuntimeException
	 */
	public function save(ActiveRecord $record) : bool
	{
		try
		{
			if(!$record->save())
			{
				$errors = [];
				
				foreach((array) $record->getErrorSummary(true) as $error)
				{
					$errors[] = (string) $error;
				}
				
				$pkeys = [];
				
				foreach((array) $record->getPrimaryKey(true) as $pkeyval)
				{
					$pkeys[] = (string) $pkeyval;
				}
				
				$message = "Failed to save {class} {id} : {errs}\n{obj}\n{old}";
				$context = [
					'{class}' => \get_class($record),
					'{id}' => \implode('|', $pkeys),
					'{errs}' => \implode(',', $errors),
					'{obj}' => \json_encode($record->getAttributes(), \JSON_PRETTY_PRINT),
					'{old}' => \json_encode($record->getOldAttributes(), \JSON_PRETTY_PRINT),
				];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		catch(\yii\db\Exception $exc)
		{
			$pkeys = [];
			
			foreach((array) $record->getPrimaryKey(true) as $pkeyval)
			{
				$pkeys[] = (string) $pkeyval;
			}
			
			$message = 'Failed to save {class} {id} : {exc}';
			$context = [
				'{class}' => \get_class($record),
				'{id}' => \implode('|', $pkeys),
				'{exc}' => $exc->getMessage(),
			];
			
			throw new RuntimeException(\strtr($message, $context), (int) $exc->getCode(), $exc);
		}
		
		return true;
	}
	
}
