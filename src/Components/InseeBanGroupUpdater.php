<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanIgnGroup;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLineInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeBan\Models\InseeBanGroup;
use Yii2Module\Yii2InseeBan\Models\InseeBanGroupHistory;
use Yii2Module\Yii2InseeBan\Models\InseeBanMetadata;

/**
 * InseeBanGroupUpdater class file.
 * 
 * This class updates the InseeBanGroup and InseeBanGroupHistory records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeBanGroupUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Gets the known ign group ids.
	 * [string:ban_id => string:ign_id].
	 * 
	 * @var array<string, string>
	 */
	protected array $_knownIgnGroupIds = [];
	
	/**
	 * The splitter of type voies.
	 * 
	 * @var InseeBanSplitter
	 */
	protected InseeBanSplitter $_splitter;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 * @param InseeBanSplitter $splitter
	 */
	public function __construct(LoggerInterface $logger, InseeBanSplitter $splitter)
	{
		$this->_logger = $logger;
		$this->_splitter = $splitter;
	}
	
	/**
	 * Updates all the ban groups for all departements and all dates.
	 * 
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeBanEndpointInterface $endpoint, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateAllDate($endpoint, $date, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban groups for the given department and all dates.
	 * 
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDepartements(ApiFrInseeBanEndpointInterface $endpoint, string $departementId, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban groups for the given date and all departements.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDate(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, bool $force = false) : int
	{
		$now = new DateTimeImmutable();
		if($date->format('Y-m-d') === $now->format('Y-m-d'))
		{
			$date = $endpoint->getLatestDate();
		}
		
		$count = 0;
		
		foreach($endpoint->getDepartementIds($date) as $departementId)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban groups for the given date and departement.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateDepartement(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, string $departementId, bool $force = false) : int
	{
		$this->_logger->info(
			'Processing Ban Groups from Departement {did} at date {date}',
			['did' => $departementId, 'date' => $date->format('Y-m-d')],
		);
		
		$ibmd = InseeBanMetadata::findOne('insee_ban_group.'.$departementId);
		if(!$force && null !== $ibmd)
		{
			$mdd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $ibmd->contents);
			if(false !== $mdd && $mdd->getTimestamp() > $date->getTimestamp())
			{
				return 0;
			}
		}
		
		$mdcid = 'insee_ban_group.'.$departementId.'.'.$date->format('Y-m');
		$mdc = InseeBanMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeBanMetadata();
			$mdc->insee_ban_metadata_id = $mdcid;
			$mdc->contents = '0';
			$mdc->save();
		}
		
		$this->collectIgnGroupIds($endpoint, $date, $departementId);
		
		$count = 0;
		$knownpc = new InseeBanKnownPostalCodes();
		
		foreach($endpoint->getBanLinesIterator($date, $departementId) as $k => $banLine)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Ban Group Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Ban Group Lines...', ['k' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$mdc->save();
			}
			
			$count += $this->updateBanLine($knownpc, $banLine);
		}
		
		if(null === $ibmd)
		{
			$ibmd = new InseeBanMetadata();
			$ibmd->insee_ban_metadata_id = 'insee_ban_group.'.$departementId;
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $ibmd->contents);
		if(empty($dti) || $dti->getTimestamp() < $date->getTimestamp())
		{
			$ibmd->contents = $date->format('Y-m-d');
			$ibmd->save();
		}
		
		return $count;
	}
	
	/**
	 * Collects the ign group ids for the given departement and date.
	 * 
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param string $departementId
	 * @throws \yii\db\Exception
	 * @throws RuntimeException
	 */
	public function collectIgnGroupIds(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, string $departementId) : void
	{
		$this->_knownIgnGroupIds = [];
		
		/** @var ApiFrInseeBanIgnGroup $groupLine */
		foreach($endpoint->getBanIgnGroupsIterator($date, $departementId) as $groupLine)
		{
			$this->_knownIgnGroupIds[(string) $groupLine->getIdBanGroup()] = (string) $groupLine->getIdIgnGroup();
		}
	}
	
	/**
	 * Updates all the ban lines for the given ban line.
	 *
	 * @param InseeBanKnownPostalCodes $knownpc
	 * @param ApiFrInseeBanLineInterface $banLine
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateBanLine(InseeBanKnownPostalCodes $knownpc, ApiFrInseeBanLineInterface $banLine) : int
	{
		$codePostal = $knownpc->getPostalCode($banLine);
		$code = ((string) $banLine->getCodeInsee()).$codePostal;
		
		$saved = 0;
		$id = $this->b64usFromHex(\str_replace('ban-group-', '', (string) $banLine->getIdBanGroup()).'0'.$codePostal);
		$fkey = $this->b64usFromHex($code);
		$fantoirId = $banLine->getIdFantoir();
		if('' === $fantoirId)
		{
			$fantoirId = null;
		}
		$ignGroupId = null;
		if(isset($this->_knownIgnGroupIds[$banLine->getIdBanGroup()]))
		{
			$ignGroupId = $this->_knownIgnGroupIds[$banLine->getIdBanGroup()];
		}
		
		$nomComplementaire = $banLine->getNomComplementaire();
		if('' === $nomComplementaire)
		{
			$nomComplementaire = null;
		}
		$inseeBanGroup = InseeBanGroup::findOne($id);
		if(null === $inseeBanGroup)
		{
			$inseeBanGroup = new InseeBanGroup();
			$inseeBanGroup->insee_ban_group_id = $id;
			$inseeBanGroup->insee_ban_postal_area_id = $fkey;
			$inseeBanGroup->date_last_update = $banLine->getDateDerMajGroup()->format('Y-m-d');
			$inseeBanGroup->fantoir_id = $fantoirId;
			$inseeBanGroup->ign_group_id = $ignGroupId;
			
			$split = $this->_splitter->split($banLine->getNomVoie());
			$inseeBanGroup->insee_ban_type_voie_id = (int) $split->getTypeVoie()->getId();
			$inseeBanGroup->way_name = $split->getRemaining();
			$inseeBanGroup->complement_name = $nomComplementaire;
			if(!$inseeBanGroup->save())
			{
				$errors = [];
				
				foreach((array) $inseeBanGroup->getErrorSummary(true) as $error)
				{
					$errors[] = (string) $error;
				}
				
				$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
				$context = [
					'{class}' => \get_class($inseeBanGroup),
					'{errs}' => \implode(',', $errors),
					'{obj}' => \json_encode($inseeBanGroup->getAttributes(), \JSON_PRETTY_PRINT),
					'{old}' => \json_encode($inseeBanGroup->getOldAttributes(), \JSON_PRETTY_PRINT),
				];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			$saved++;
		}
		else
		{
			$blu = DateTimeImmutable::createFromFormat('Y-m-d', $inseeBanGroup->date_last_update);
			if(false === $blu)
			{
				$message = 'Failed to parse date last update "{dt}"';
				$context = ['{dt}' => $inseeBanGroup->date_last_update];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			$llu = $banLine->getDateDerMajGroup();
			$bludays = 12 * 32 * ((int) $blu->format('Y')) + 32 * ((int) $blu->format('m') - 1) + ((int) $blu->format('d'));
			$lludays = 12 * 32 * ((int) $llu->format('Y')) + 32 * ((int) $llu->format('m') - 1) + ((int) $llu->format('d'));
			$mustBeUpdated = $lludays > $bludays;
			$split = $this->_splitter->split($banLine->getNomVoie());
			
			$mustBeSaved = (string) $inseeBanGroup->insee_ban_postal_area_id !== (string) $fkey
				|| (string) $inseeBanGroup->fantoir_id !== (string) $fantoirId
				|| (string) $inseeBanGroup->ign_group_id !== (string) $ignGroupId
				|| (string) $inseeBanGroup->insee_ban_type_voie_id !== (string) $split->getTypeVoie()->getId()
				|| (string) $inseeBanGroup->way_name !== (string) $split->getRemaining()
				|| (string) $inseeBanGroup->complement_name !== (string) $nomComplementaire;
			
			if($mustBeSaved)
			{
				$saved += (int) $this->saveObjectClass(InseeBanGroupHistory::class, [
					'insee_ban_group_id' => $inseeBanGroup->insee_ban_group_id,
					'date_end_validity' => $inseeBanGroup->date_last_update,
				], [
					'insee_ban_postal_area_id' => $inseeBanGroup->insee_ban_postal_area_id,
					'fantoir_id' => $inseeBanGroup->fantoir_id,
					'ign_group_id' => $inseeBanGroup->ign_group_id,
					'insee_ban_type_voie_id' => $inseeBanGroup->insee_ban_type_voie_id,
					'way_name' => $inseeBanGroup->way_name,
					'complement_name' => $inseeBanGroup->complement_name,
				])->isNewRecord;
				
				$inseeBanGroup->date_last_update = $banLine->getDateDerMajGroup()->format('Y-m-d');
				$inseeBanGroup->insee_ban_postal_area_id = $fkey;
				$inseeBanGroup->fantoir_id = $fantoirId;
				$inseeBanGroup->ign_group_id = $ignGroupId;
				$inseeBanGroup->insee_ban_type_voie_id = (int) $split->getTypeVoie()->getId();
				$inseeBanGroup->way_name = $split->getRemaining();
				$inseeBanGroup->complement_name = $nomComplementaire;
				if(!$inseeBanGroup->save())
				{
					$errors = [];
					
					foreach((array) $inseeBanGroup->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($inseeBanGroup),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($inseeBanGroup->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($inseeBanGroup->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$saved++;
			}
			elseif($mustBeUpdated)
			{
				$inseeBanGroup->date_last_update = $banLine->getDateDerMajGroup()->format('Y-m-d');
				if(!$inseeBanGroup->save())
				{
					$errors = [];
					
					foreach((array) $inseeBanGroup->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($inseeBanGroup),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($inseeBanGroup->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($inseeBanGroup->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$saved++;
			}
		}
		
		return $saved;
	}
	
	/**
	 * Realigns the groups with the type voies.
	 * 
	 * @param boolean $force
	 * @return integer the number of records modified
	 * @throws \yii\db\Exception
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function realignGroupWithTypeVoie(bool $force = false) : int
	{
		$query = InseeBanGroup::find();
		if(!$force)
		{
			$query = $query->andWhere('insee_ban_type_voie_id IS NULL');
		}
		$count = $query->count();
		$processed = 0;
		$updated = 0;
		$offset = 0;
		$limit = 1000;
		
		$parser = $this->_splitter;
		
		while($offset < $count)
		{
			$query = InseeBanGroup::find()->limit($limit);
			if(!$force)
			{
				$query = $query->andWhere('insee_ban_type_voie_id IS NULL');
			}
			else
			{
				$query = $query->offset($offset);
			}
			$groups = $query->all();
			$offset += $limit;
			
			/** @var InseeBanGroup $group */
			foreach($groups as $group)
			{
				$processed++;
				$typeVoie = $group->inseeBanTypeVoie;
				$voieName = $typeVoie->description.' '.$group->way_name;
				$splitted = $parser->split($voieName);
				
				$this->_logger->info('[{k}/{c}] Processing address {aid}, {agr} {aname} into {ngr} {nname}', [
					'k' => $processed,
					'c' => $count,
					'aid' => $group->insee_ban_group_id,
					'agr' => $typeVoie->description,
					'aname' => $group->way_name,
					'ngr' => $splitted->getTypeVoie()->getName(),
					'nname' => $splitted->getRemaining(),
				]);
				
				$group->insee_ban_type_voie_id = (int) $splitted->getTypeVoie()->getId();
				$group->way_name = $splitted->getRemaining();
				
				if(empty($group->way_name))
				{
					$group->way_name = (string) $splitted->getTypeVoie()->getName();
				}
				
				if(!$group->save())
				{
					$errors = [];
					
					foreach((array) $group->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($group),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($group->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($group->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$updated++;
			}
		}
		
		return $updated;
	}
	
	/**
	 * Realigns the group historys with the type voies.
	 * 
	 * @param boolean $force
	 * @return integer the number of records modified
	 * @throws \yii\db\Exception
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function realignGroupHistoryWithTypeVoie(bool $force = false) : int
	{
		$query = InseeBanGroupHistory::find();
		if(!$force)
		{
			$query = $query->andWhere('insee_ban_type_voie_id IS NULL');
		}
		$count = $query->count();
		$processed = 0;
		$updated = 0;
		$offset = 0;
		$limit = 1000;
		
		$parser = $this->_splitter;
		
		while($offset < $count)
		{
			$query = InseeBanGroupHistory::find()->limit($limit);
			if(!$force)
			{
				$query = $query->andWhere('insee_ban_type_voie_id IS NULL');
			}
			else
			{
				$query = $query->offset($offset);
			}
			$groupHistories = $query->all();
			$offset += $limit;
			
			/** @var InseeBanGroupHistory $groupHistory */
			foreach($groupHistories as $groupHistory)
			{
				$processed++;
				$typeVoie = $groupHistory->inseeBanTypeVoie;
				$voieName = $typeVoie->description.' '.$groupHistory->way_name;
				$splitted = $parser->split($voieName);
				
				$this->_logger->info('[{k}/{c}] Processing address {aid}, {agr} {aname} into {ngr} {nname}', [
					'k' => $processed,
					'c' => $count,
					'aid' => $groupHistory->insee_ban_group_id,
					'agr' => $typeVoie->description,
					'aname' => $groupHistory->way_name,
					'ngr' => $splitted->getTypeVoie()->getName(),
					'nname' => $splitted->getRemaining(),
				]);
				
				$groupHistory->insee_ban_type_voie_id = (int) $splitted->getTypeVoie()->getId();
				$groupHistory->way_name = $splitted->getRemaining();
				
				if(empty($groupHistory->way_name))
				{
					$groupHistory->way_name = (string) $splitted->getTypeVoie()->getName();
				}
				
				if(!$groupHistory->save())
				{
					$errors = [];
					
					foreach((array) $groupHistory->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($groupHistory),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($groupHistory->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($groupHistory->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$updated++;
			}
		}
		
		return $updated;
	}
	
	/**
	 * Gets the base64 urlsafe encoded string of the given hexadecimal string.
	 * The hexadecimal string must have an odd number of hexadecimal digits.
	 *
	 * @param string $hexa
	 * @return string
	 */
	protected function b64usFromHex(string $hexa)
	{
		return \str_replace(['+', '/'], ['-', '_'], \trim(\base64_encode((string) \hex2bin($hexa)), '='));
	}
	
}
