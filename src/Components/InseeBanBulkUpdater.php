<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface;
use PhpExtended\Slugifier\SlugifierInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Yii2InseeBan\Models\InseeBanMetadata;
use Yii2Module\Yii2InseeCog\Models\InseeCogMetadata;
use Yii2Module\Yii2Log\Models\Log;

/**
 * InseeBanBulkUpdater class file.
 * 
 * This bulk updater updates bulk of data.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeBanBulkUpdater
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The endpoint.
	 * 
	 * @var ApiFrInseeBanEndpointInterface
	 */
	protected ApiFrInseeBanEndpointInterface $_endpoint;
	
	/**
	 * The slugifier.
	 * 
	 * @var SlugifierInterface
	 */
	protected SlugifierInterface $_slugifier;
	
	/**
	 * The splitter.
	 * 
	 * @var InseeBanSplitter
	 */
	protected InseeBanSplitter $_splitter;
	
	/**
	 * The date when this update should stop (next cron will relieve).
	 *
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_endDate;
	
	/**
	 * The count of files that must be overlooked.
	 * 
	 * @var integer
	 */
	protected int $_forwardCount = 30;
	
	/**
	 * Builds a new InseeBanBulkUpdater with the given components.
	 * 
	 * @param LoggerInterface $logger
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param SlugifierInterface $slugifier
	 * @param InseeBanSplitter $splitter
	 */
	public function __construct(LoggerInterface $logger, ApiFrInseeBanEndpointInterface $endpoint, SlugifierInterface $slugifier, InseeBanSplitter $splitter)
	{
		$this->_logger = $logger;
		$this->_endpoint = $endpoint;
		$this->_slugifier = $slugifier;
		$this->_splitter = $splitter;
		/** @psalm-suppress PossiblyFalseArgument */
		$this->_endDate = (new DateTimeImmutable())->add(DateInterval::createFromDateString('+20 hours'));
	}
	
	/**
	 * Keeps all the connections alive by trying to get an object from each
	 * connection.
	 */
	public function keepalive() : void
	{
		InseeBanMetadata::find()->one();
		InseeCogMetadata::find()->one();
		Log::find()->one();
	}
	
	/**
	 * Updates all the ban records for all the departements.
	 * 
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDepartements() : int
	{
		$date = $this->getFirstUpdatableDate();
		if(null === $date)
		{
			return 0;
		}
		
		$count = 0;
		
		foreach($this->_endpoint->getDepartementIds($date) as $departementId)
		{
			$count += $this->updateDepartement($departementId, $date);
			$now = new DateTimeImmutable();
			if($now->getTimestamp() > $this->_endDate->getTimestamp())
			{
				return $count;
			}
		}
		
		$mdd = InseeBanMetadata::findOne('insee_ban_bulk.all.date_last_import');
		if(null === $mdd)
		{
			$mdd = new InseeBanMetadata();
			$mdd->insee_ban_metadata_id = 'insee_ban_bulk.all.date_last_import';
		}
		$mdd->contents = $date->format('Y-m-d');
		$mdd->save();
		
		return $count;
	}
	
	/**
	 * Updates all the ban records for the given departement.
	 * 
	 * @param string $departementId
	 * @param ?DateTimeInterface $dateFor
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateDepartement(string $departementId, ?DateTimeInterface $dateFor = null) : int
	{
		$mdd = InseeBanMetadata::findOne('insee_ban_bulk.'.$departementId.'.date_last_import');
		$mddate = null;
		if(null !== $mdd)
		{
			$nmddate = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdd->contents);
			if(false !== $nmddate)
			{
				$mddate = $nmddate;
			}
		}
		
		// the date for is for harmonization of minimum date processing
		// amongst all the departements
		// meaning if the date of last update of the departement is more than
		// the date for, then we have to skip this department
		if(null !== $mddate && null !== $dateFor && $mddate->getTimestamp() > $dateFor->getTimestamp())
		{
			return 0;
		}
		
		$dates = $this->_endpoint->getUploadDates();
		// tri croissant
		\usort($dates, function(DateTimeInterface $dt1, DateTimeInterface $dt2) : int
		{
			return $dt1->getTimestamp() - $dt2->getTimestamp();
		});
		$lastDate = null;
		
		$gatherer = new InseeBanBulkGatherer(
			$this->_logger,
			$this->_endpoint,
			$this->_slugifier,
			$this->_splitter,
			$departementId,
		);
		
		$dtCount = 0;
		
		foreach($dates as $date)
		{
			$lastDate = $date;
			if(null !== $mddate)
			{
				if($mddate->format('Y-m-d') === $date->format('Y-m-d') || $mddate->getTimestamp() > $date->getTimestamp())
				{
					continue;
				}
			}
			
			$gatherer->gatherForDate($date);
			$this->keepalive();
			
			// do not be here all night
			$dtCount++;
			if($this->_forwardCount <= $dtCount)
			{
				break;
			}
		}
		
		$count = $gatherer->saveAll(function() : void
		{ $this->keepalive(); });
		
		if(null !== $lastDate)
		{
			if(null === $mdd)
			{
				$mdd = new InseeBanMetadata();
				$mdd->insee_ban_metadata_id = 'insee_ban_bulk.'.$departementId.'.date_last_import';
			}
			$mdd->contents = $lastDate->format('Y-m-d');
			$mdd->save();
		}
		
		$this->keepalive();
		
		return $count;
	}
	
	/**
	 * Gets the lowest date possible for uploads, that will be registered into 
	 * the metadata to check that all departements were successfully processed.
	 * 
	 * @return ?DateTimeInterface
	 * @throws RuntimeException
	 */
	public function getFirstUpdatableDate() : ?DateTimeInterface
	{
		$mddate = null;
		$mdd = InseeBanMetadata::findOne('insee_ban_bulk.all.date_last_import');
		if(null !== $mdd)
		{
			$nmddate = DateTimeImmutable::createFromFormat('Y-m-d', (string) $mdd->contents);
			if(false !== $nmddate)
			{
				$mddate = $nmddate;
			}
		}
		
		$dates = $this->_endpoint->getUploadDates();
		// tri croissant
		\usort($dates, function(DateTimeInterface $dt1, DateTimeInterface $dt2) : int
		{
			return $dt1->getTimestamp() - $dt2->getTimestamp();
		});
		
		if(null === $mddate)
		{
			return \array_shift($dates);
		}
		
		foreach($dates as $date)
		{
			// not the same day and timestamp more recent
			if($date->format('Y-m-d') !== $mddate->format('Y-m-d')
				&& $date->getTimestamp() > $mddate->getTimestamp()
			) {
				return $date;
			}
		}
		
		return null;
	}
	
}
