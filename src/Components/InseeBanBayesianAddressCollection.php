<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use ArrayIterator;
use Countable;
use Iterator;
use IteratorAggregate;
use Yii2Module\Yii2InseeBan\Models\InseeBanAddress;

/**
 * InseeBanBayesianAddressCollection class file.
 * 
 * This class represents a collection that handles search results over address.
 * 
 * @author Anastaszor
 * @implements \IteratorAggregate<integer, InseeBanBayesianAddress>
 */
class InseeBanBayesianAddressCollection implements Countable, IteratorAggregate
{
	
	/**
	 * The default number of records to keep in a single collection for each
	 * surgroup (here the number of addresses per group).
	 * 
	 * @var integer
	 */
	public const DEFAULT_MAX_RECORDS_PER_GROUP = 7;
	
	/**
	 * The max number of records to keep in a single collection per group.
	 * 
	 * @var integer
	 */
	protected int $_maxRecordsPerGroup = self::DEFAULT_MAX_RECORDS_PER_GROUP;
	
	/**
	 * The data from the address.
	 * 
	 * @var array<integer, InseeBanBayesianAddress>
	 */
	protected array $_data = [];
	
	/**
	 * Builds a new InseeBanBayesianAddressCollection with the given quantity of
	 * records to hold.
	 * 
	 * @param int $maxRecordsPerGroup
	 */
	public function __construct(int $maxRecordsPerGroup = self::DEFAULT_MAX_RECORDS_PER_GROUP)
	{
		$this->_maxRecordsPerGroup = $maxRecordsPerGroup;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorAggregate::getIterator()
	 */
	public function getIterator() : Iterator
	{
		return new ArrayIterator($this->_data);
	}
	
	/**
	 * Adds a new bayesian address to the collection.
	 * 
	 * @param InseeBanBayesianAddress $address
	 * @return InseeBanBayesianAddressCollection
	 */
	public function add(InseeBanBayesianAddress $address) : InseeBanBayesianAddressCollection
	{
		$countRecordsForGroup = 0;
		
		/** @var InseeBanBayesianAddress $record */
		foreach($this->_data as $record)
		{
			$countRecordsForGroup += (int) ($record->getBayesianGroup()->getGroup()->insee_ban_group_id === $address->getBayesianGroup()->getGroup()->insee_ban_group_id);
		}
		
		if(0 < $this->_maxRecordsPerGroup && $countRecordsForGroup > $this->_maxRecordsPerGroup)
		{
			$rmvVal = $address->getTotalFitness();
			$rmvIdx = null; // null we remove nothing, else remove a record
			
			/** @var InseeBanBayesianAddress $record */
			foreach($this->_data as $k => $record)
			{
				if($record->getBayesianGroup()->getGroup()->insee_ban_group_id === $address->getBayesianGroup()->getGroup()->insee_ban_group_id)
				{
					$fitness = $record->getTotalFitness();
					if($fitness < $rmvVal)
					{
						$rmvVal = $fitness;
						$rmvIdx = $k;
					}
				}
			}
			
			if(null === $rmvIdx)
			{
				return $this;
			}
			
			unset($this->_data[$rmvIdx]);
		}
		
		$this->_data[] = $address;
		
		return $this;
	}
	
	/**
	 * Adds a new address and its fitness value to the collection.
	 * 
	 * @param InseeBanBayesianGroup $bayesianGroup
	 * @param InseeBanAddress $address
	 * @param float $fitness
	 * @return InseeBanBayesianAddressCollection
	 */
	public function addp(InseeBanBayesianGroup $bayesianGroup, InseeBanAddress $address, float $fitness) : InseeBanBayesianAddressCollection
	{
		return $this->add(new InseeBanBayesianAddress($bayesianGroup, $address, $fitness));
	}
	
	/**
	 * Gets whether this list is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool
	{
		return \count($this->_data) === 0;
	}
	
	/**
	 * Gets the best fit from the collection.
	 * 
	 * @return ?InseeBanBayesianAddress
	 */
	public function getBestFit() : ?InseeBanBayesianAddress
	{
		$bestAddr = null;
		$bestScore = 0;
		
		/** @var InseeBanBayesianAddress $addr */
		foreach($this->_data as $addr)
		{
			$totalFitness = $addr->getTotalFitness();
			if($totalFitness > $bestScore)
			{
				$bestScore = $totalFitness;
				$bestAddr = $addr;
			}
		}
		
		return $bestAddr;
	}
	
}
