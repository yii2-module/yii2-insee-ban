<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeBan\Models\InseeBanTypeVoie;

/**
 * InseeBanTypeVoieUpdater class file.
 * 
 * This class updates the InseeBanTypeVoie records.
 * 
 * @author Anastaszor
 */
class InseeBanTypeVoieUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new InseeBanTypeVoieUpdater.
	 * 
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the ban type voies.
	 * 
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param ?DateTimeInterface $stopAt
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function updateAll(ApiFrInseeBanEndpointInterface $endpoint, ?DateTimeInterface $stopAt = null) : int
	{
		$this->_logger->info('Processing Ban Types Voies');
		
		$count = 0;
		
		/** @var \PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoieInterface $typeVoie */
		foreach($endpoint->getTypeVoieIterator() as $k => $typeVoie)
		{
			if(0 === ((int) $k + 1) % 1000)
			{
				$this->_logger->info('Processed {k} Types Voies', ['k' => (int) $k + 1]);
				
				if(null !== $stopAt && \time() > $stopAt->getTimestamp())
				{
					return $count;
				}
			}
			
			$count += (int) $this->saveObjectClass(InseeBanTypeVoie::class, [
				'insee_ban_type_voie_id' => (int) $typeVoie->getId(),
			], [
				'code' => $typeVoie->getCode(),
				'description' => $typeVoie->getName(),
			])->isNewRecord;
		}
		
		return $count;
	}
	
}
