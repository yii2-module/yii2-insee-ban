<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeBan\Models\InseeBanLocalisation;
use Yii2Module\Yii2InseeBan\Models\InseeBanMetadata;

/**
 * InseeBanLocalisationUpdater class file.
 * 
 * This class updates all the InseeBanLocalisation from the endpoint.
 * 
 * @author Anastaszor
 */
class InseeBanLocalisationUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the records.
	 * 
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param boolean $force
	 * @return integer the number of records updated
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeBanEndpointInterface $endpoint, bool $force = false) : int
	{
		$this->_logger->info('Processing Localisations');
		
		$ibmd = InseeBanMetadata::findOne('insee_ban_localisations');
		if(!$force && null !== $ibmd && 'true' === $ibmd->contents)
		{
			return 0;
		}
		
		$count = 0;
		
		/** @var \PhpExtended\ApiFrInseeBan\ApiFrInseeBanLocalisationInterface $localisation */
		foreach($endpoint->getBanLocalisationIterator() as $localisation)
		{
			$count += (int) $this->saveObjectClass(InseeBanLocalisation::class, [
				'insee_ban_localisation_id' => (int) $localisation->getId(),
			], [
				'code' => $localisation->getCode(),
				'precision' => $localisation->getPrecision(),
				'description' => $localisation->getDefinition(),
			])->isNewRecord;
		}
		
		if(null === $ibmd)
		{
			$ibmd = new InseeBanMetadata();
			$ibmd->insee_ban_metadata_id = 'insee_ban_localisations';
		}
		$ibmd->contents = 'true';
		$ibmd->save();
		
		return $count;
	}
	
}
