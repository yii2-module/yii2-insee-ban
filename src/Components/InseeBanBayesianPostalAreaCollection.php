<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use ArrayIterator;
use Countable;
use Iterator;
use IteratorAggregate;
use PhpExtended\Slugifier\SlugifierInterface;
use Yii2Module\Yii2InseeBan\Models\InseeBanPostalArea;

/**
 * InseeBanBayesianPostalAreaCollection class file.
 * 
 * This class represnts a collection that handles search results over postal
 * areas.
 * 
 * @author Anastaszor
 * @implements \IteratorAggregate<integer, InseeBanBayesianPostalArea>
 */
class InseeBanBayesianPostalAreaCollection implements Countable, IteratorAggregate
{
	
	/**
	 * The default number of records to keep in a single collection.
	 * 
	 * @var integer
	 */
	public const DEFAULT_MAX_RECORDS = 7;
	
	/**
	 * The max number of records to keep in a single collection.
	 * 
	 * @var integer
	 */
	protected int $_maxRecords = self::DEFAULT_MAX_RECORDS;
	
	/**
	 * The slugifier.
	 * 
	 * @var SlugifierInterface
	 */
	protected SlugifierInterface $_slugifier;
	
	/**
	 * The data from the postal areas.
	 * 
	 * @var array<integer, InseeBanBayesianPostalArea>
	 */
	protected array $_data = [];
	
	/**
	 * Builds a new InseeBanBayesianPostalAreaCollection with the given quantity
	 * of records to hold.
	 * 
	 * @param SlugifierInterface $slugifier
	 * @param integer $maxRecords
	 */
	public function __construct(SlugifierInterface $slugifier, int $maxRecords = self::DEFAULT_MAX_RECORDS)
	{
		$this->_slugifier = $slugifier;
		$this->_maxRecords = $maxRecords;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorAggregate::getIterator()
	 */
	public function getIterator() : Iterator
	{
		return new ArrayIterator($this->_data);
	}
	
	/**
	 * Absorbs the given ban postal areas from the given iterator.
	 * 
	 * @param array<integer, InseeBanPostalArea> $areas
	 * @param ?string $refLibelleCommune
	 * @param ?string $refCodeCommune
	 * @param ?string $refCodePostal
	 * @return InseeBanBayesianPostalAreaCollection
	 */
	public function absorbAll(array $areas, ?string $refLibelleCommune, ?string $refCodeCommune, ?string $refCodePostal) : InseeBanBayesianPostalAreaCollection
	{
		foreach($areas as $area)
		{
			$this->absorb($area, $refLibelleCommune, $refCodeCommune, $refCodePostal);
		}
		
		return $this;
	}
	
	/**
	 * Absorbs the given ban postal area with the given score.
	 * 
	 * @param InseeBanPostalArea $area
	 * @param ?string $refLibelleCommune
	 * @param ?string $refCodeCommune
	 * @param ?string $refCodePostal
	 * @return InseeBanBayesianPostalAreaCollection
	 */
	public function absorb(InseeBanPostalArea $area, ?string $refLibelleCommune, ?string $refCodeCommune, ?string $refCodePostal) : InseeBanBayesianPostalAreaCollection
	{
		$score1 = null === $refLibelleCommune || '' === $refLibelleCommune ? 1.0 : $this->getScore($refLibelleCommune, $area->name);
		$score2 = null === $refCodeCommune || '' === $refCodeCommune ? 1.0 : $this->getScore($refCodeCommune, $area->insee_ban_postal_area_id);
		$score3 = null === $refCodePostal || '' === $refCodePostal ? 1.0 : $this->getScore($refCodePostal, $area->postal_code);
		
		return $this->addp($area, $score1 * $score2 * $score3);
	}
	
	/**
	 * Gets the score from levenshtein distance between two strings.
	 * 1 is the best score, and 0 is the worst score.
	 * 
	 * @param ?string $reference
	 * @param ?string $challenger
	 * @return float [0,1]
	 */
	public function getScore(?string $reference, ?string $challenger) : float
	{
		$reference = $this->_slugifier->slugify((string) $reference);
		$challenger = $this->_slugifier->slugify((string) $challenger);
		
		$denom = (int) \max(1, (string) \mb_strlen($challenger, '8bit'), (string) \mb_strlen($reference, '8bit'));
		$diff = $denom - \str_levenshtein($reference, $challenger);
		
		return ((float) $diff) / ((float) $denom);
	}
	
	/**
	 * Adds a new bayesian postal area to the collection.
	 * 
	 * @param InseeBanBayesianPostalArea $area
	 * @return InseeBanBayesianPostalAreaCollection
	 */
	public function add(InseeBanBayesianPostalArea $area) : InseeBanBayesianPostalAreaCollection
	{
		if(0 < $this->_maxRecords && \count($this->_data) > $this->_maxRecords)
		{
			$rmvVal = $area->getTotalFitness();
			$rmvIdx = null; // null we remove nothing, else remove a record
			
			/** @var InseeBanBayesianPostalArea $record */
			foreach($this->_data as $k => $record)
			{
				$fitness = $record->getTotalFitness();
				if($fitness < $rmvVal)
				{
					$rmvVal = $fitness;
					$rmvIdx = $k;
				}
			}
			
			if(null === $rmvIdx)
			{
				return $this;
			}
			
			unset($this->_data[$rmvIdx]);
		}
		
		$this->_data[] = $area;
		
		return $this;
	}
	
	/**
	 * Adds a new postal area and its fitness value to the collection.
	 *
	 * @param InseeBanPostalArea $area
	 * @param float $fitness
	 * @return InseeBanBayesianPostalAreaCollection
	 */
	public function addp(InseeBanPostalArea $area, float $fitness) : InseeBanBayesianPostalAreaCollection
	{
		return $this->add(new InseeBanBayesianPostalArea($area, $fitness));
	}
	
	/**
	 * Gets whether this list is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool
	{
		return \count($this->_data) === 0;
	}
	
	/**
	 * Gets the best fit from the collection.
	 * 
	 * @return ?InseeBanBayesianPostalArea
	 */
	public function getBestFit() : ?InseeBanBayesianPostalArea
	{
		$bestArea = null;
		$bestScore = 0;
		
		/** @var InseeBanBayesianPostalArea $area */
		foreach($this->_data as $area)
		{
			$totalFitness = $area->getTotalFitness();
			if($totalFitness > $bestScore)
			{
				$bestScore = $totalFitness;
				$bestArea = $area;
			}
		}
		
		return $bestArea;
	}
	
}
