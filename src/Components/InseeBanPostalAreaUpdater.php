<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLineInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeBan\Models\InseeBanMetadata;
use Yii2Module\Yii2InseeBan\Models\InseeBanPostalArea;
use Yii2Module\Yii2InseeBan\Models\InseeBanPostalAreaHistory;

/**
 * InseeBanPostalAreaUpdater class file.
 * 
 * This class updates the InseeBanPostalArea and InseeBanPostalAreaHistory records.
 * 
 * @author Anastaszor
 */
class InseeBanPostalAreaUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 * 
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the ban postal areas for all departements and all dates.
	 * 
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeBanEndpointInterface $endpoint, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateAllDate($endpoint, $date, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban postal areas for the given department and all dates.
	 * 
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDepartements(ApiFrInseeBanEndpointInterface $endpoint, string $departementId, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban postal areas for the given date and all departements.
	 * 
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDate(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, bool $force = false) : int
	{
		$now = new DateTimeImmutable();
		if($date->format('Y-m-d') === $now->format('Y-m-d'))
		{
			$date = $endpoint->getLatestDate();
		}
		
		$count = 0;
		
		foreach($endpoint->getDepartementIds($date) as $departementId)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban postal areas for the given date and departement.
	 * 
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateDepartement(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, string $departementId, bool $force = false) : int
	{
		$this->_logger->info(
			'Processing Ban Postal Areas from Departement {did} at date {date}',
			['did' => $departementId, 'date' => $date->format('Y-m-d')],
		);
		
		$ibmd = InseeBanMetadata::findOne('insee_ban_postal_area.'.$departementId);
		if(!$force && null !== $ibmd)
		{
			$mdd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $ibmd->contents);
			if(false !== $mdd && $mdd->getTimestamp() > $date->getTimestamp())
			{
				return 0;
			}
		}
		
		$mdcid = 'insee_ban_postal_area.'.$departementId.'.'.$date->format('Y-m');
		$mdc = InseeBanMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeBanMetadata();
			$mdc->insee_ban_metadata_id = $mdcid;
			$mdc->contents = '0';
			$mdc->save();
		}
		
		$count = 0;
		$knownpc = new InseeBanKnownPostalCodes();
		
		foreach($endpoint->getBanLinesIterator($date, $departementId) as $k => $banLine)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Ban Postal Area Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Ban Postal Area Lines...', ['k' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$mdc->save();
			}
			
			$count += $this->updateBanLine($knownpc, $banLine);
		}
		
		if(null === $ibmd)
		{
			$ibmd = new InseeBanMetadata();
			$ibmd->insee_ban_metadata_id = 'insee_ban_postal_area.'.$departementId;
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $ibmd->contents);
		if(empty($dti) || $dti->getTimestamp() < $date->getTimestamp())
		{
			$ibmd->contents = $date->format('Y-m-d');
			$ibmd->save();
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban lines for the given ban line.
	 * 
	 * @param InseeBanKnownPostalCodes $knownpc
	 * @param ApiFrInseeBanLineInterface $banLine
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function updateBanLine(InseeBanKnownPostalCodes $knownpc, ApiFrInseeBanLineInterface $banLine) : int
	{
		$codePostal = $knownpc->getPostalCode($banLine);
		$code = ((string) $banLine->getCodeInsee()).$codePostal;
		
		$saved = 0;
		$id = $this->b64usFromHex($code);
		$inseeBanPostalArea = InseeBanPostalArea::findOne($id);
		if(null === $inseeBanPostalArea)
		{
			$inseeBanPostalArea = new InseeBanPostalArea();
			$inseeBanPostalArea->insee_ban_postal_area_id = $id;
			$inseeBanPostalArea->insee_cog_commune_id = (string) $banLine->getCodeInsee();
			$inseeBanPostalArea->date_last_update = $banLine->getDateDerMajGroup()->format('Y-m-d');
			$inseeBanPostalArea->postal_code = $codePostal;
			$inseeBanPostalArea->name = (string) $banLine->getNomCommune();
			if(!$inseeBanPostalArea->save())
			{
				$errors = [];
				
				foreach((array) $inseeBanPostalArea->getErrorSummary(true) as $error)
				{
					$errors[] = (string) $error;
				}
				
				$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
				$context = [
					'{class}' => \get_class($inseeBanPostalArea),
					'{errs}' => \implode(',', $errors),
					'{obj}' => \json_encode($inseeBanPostalArea->getAttributes(), \JSON_PRETTY_PRINT),
					'{old}' => \json_encode($inseeBanPostalArea->getOldAttributes(), \JSON_PRETTY_PRINT),
				];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			$saved++;
		}
		else
		{
			$blu = DateTimeImmutable::createFromFormat('Y-m-d', $inseeBanPostalArea->date_last_update);
			if(false === $blu)
			{
				$message = 'Failed to parse date last update "{dt}"';
				$context = ['{dt}' => $inseeBanPostalArea->date_last_update];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			$llu = $banLine->getDateDerMajGroup();
			$bludays = 12 * 32 * ((int) $blu->format('Y')) + 32 * ((int) $blu->format('m') - 1) + ((int) $blu->format('d'));
			$lludays = 12 * 32 * ((int) $llu->format('Y')) + 32 * ((int) $llu->format('m') - 1) + ((int) $llu->format('d'));
			$mustBeUpdated = $lludays > $bludays;
			
			$mustBeSaved = ($banLine->getCodeInsee() !== ((string) $inseeBanPostalArea->insee_cog_commune_id))
				|| (((string) $inseeBanPostalArea->postal_code) !== $codePostal)
				|| ($banLine->getNomCommune() !== ((string) $inseeBanPostalArea->name));
			
			if($mustBeSaved)
			{
				$saved += (int) $this->saveObjectClass(InseeBanPostalAreaHistory::class, [
					'insee_ban_postal_area_id' => $inseeBanPostalArea->insee_ban_postal_area_id,
					'date_end_validity' => $inseeBanPostalArea->date_last_update,
				], [
					'insee_cog_commune_id' => $inseeBanPostalArea->insee_cog_commune_id,
					'postal_code' => $inseeBanPostalArea->postal_code,
					'name' => $inseeBanPostalArea->name,
				])->isNewRecord;
				
				$inseeBanPostalArea->date_last_update = $banLine->getDateDerMajGroup()->format('Y-m-d');
				$inseeBanPostalArea->insee_cog_commune_id = (string) $banLine->getCodeInsee();
				$inseeBanPostalArea->postal_code = $codePostal;
				$inseeBanPostalArea->name = (string) $banLine->getNomCommune();
				if(!$inseeBanPostalArea->save())
				{
					$errors = [];
					
					foreach((array) $inseeBanPostalArea->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($inseeBanPostalArea),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($inseeBanPostalArea->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($inseeBanPostalArea->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$saved++;
			}
			elseif($mustBeUpdated)
			{
				$inseeBanPostalArea->date_last_update = $banLine->getDateDerMajGroup()->format('Y-m-d');
				if(!$inseeBanPostalArea->save())
				{
					$errors = [];
					
					foreach((array) $inseeBanPostalArea->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($inseeBanPostalArea),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($inseeBanPostalArea->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($inseeBanPostalArea->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$saved++;
			}
		}
		
		return $saved;
	}
	
	/**
	 * Gets the base64 urlsafe encoded string of the given hexadecimal string.
	 * The hexadecimal string must have an odd number of hexadecimal digits.
	 * 
	 * @param string $hexa
	 * @return string
	 */
	protected function b64usFromHex(string $hexa)
	{
		return \str_replace(['+', '/'], ['-', '_'], \trim(\base64_encode((string) \hex2bin($hexa)), '='));
	}
	
}
