<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use Yii2Module\Yii2InseeBan\Models\InseeBanPostalArea;

/**
 * InseeBanBayesianPostalArea class file.
 * 
 * This class represents a search result over a postal area with its given
 * fitness.
 * 
 * @author Anastaszor
 */
class InseeBanBayesianPostalArea
{
	
	/**
	 * The targeted area.
	 * 
	 * @var InseeBanPostalArea
	 */
	protected InseeBanPostalArea $_area;
	
	/**
	 * The fitness value of the postal area.
	 * 
	 * @var float [0,1]
	 */
	protected float $_fitness;
	
	/**
	 * Builds a new InseeBanBayesianPostalArea with its fitness value.
	 * 
	 * @param InseeBanPostalArea $area
	 * @param float $fitness
	 */
	public function __construct(InseeBanPostalArea $area, float $fitness)
	{
		$this->_area = $area;
		$this->_fitness = \min(1.0, \max(0.0, $fitness));
	}
	
	/**
	 * Gets the postal area.
	 * 
	 * @return InseeBanPostalArea
	 */
	public function getArea() : InseeBanPostalArea
	{
		return $this->_area;
	}
	
	/**
	 * Gets the fitness of the value, as probability.
	 * 
	 * @return float
	 */
	public function getFitness() : float
	{
		return $this->_fitness;
	}
	
	/**
	 * Gets the fitness for the whole bayesian chain.
	 * 
	 * @return float
	 */
	public function getTotalFitness() : float
	{
		return $this->getFitness();
	}
	
}
