<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanIgnAddress;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLineInterface;
use PhpExtended\Slugifier\SlugifierFactory;
use PhpExtended\Slugifier\SlugifierInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeBan\Models\InseeBanAddress;
use Yii2Module\Yii2InseeBan\Models\InseeBanAddressHistory;
use Yii2Module\Yii2InseeBan\Models\InseeBanMetadata;

/**
 * InseeBanAddressUpdater class file.
 * 
 * This class updates the InseeBanAddress and InseeBanAddressHistory records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeBanAddressUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The slugifier.
	 * 
	 * @var SlugifierInterface
	 */
	protected SlugifierInterface $_slugifier;
	
	/**
	 * Gets the known ign address ids.
	 * [string:ban_id => string:ign_id].
	 * 
	 * @var array<string, string>
	 */
	protected array $_knownIgnAddressIds = [];
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
		$factory = new SlugifierFactory();
		$this->_slugifier = $factory->createSlugifier();
	}
	
	/**
	 * Updates all the ban addresses for all departements and all dates.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeBanEndpointInterface $endpoint, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateAllDate($endpoint, $date, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban addresses for the given department and all dates.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDepartements(ApiFrInseeBanEndpointInterface $endpoint, string $departementId, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban addresses for the given date and all departements.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDate(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, bool $force = false) : int
	{
		$now = new DateTimeImmutable();
		if($date->format('Y-m-d') === $now->format('Y-m-d'))
		{
			$date = $endpoint->getLatestDate();
		}
		$count = 0;
		
		foreach($endpoint->getDepartementIds($date) as $departementId)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban addresses for the given date and departement.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateDepartement(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, string $departementId, bool $force = false) : int
	{
		$this->_logger->info(
			'Processing Ban Addresses from Departement {did} at date {date}',
			['did' => $departementId, 'date' => $date->format('Y-m-d')],
		);
		
		$ibmd = InseeBanMetadata::findOne('insee_ban_address.'.$departementId);
		if(!$force && null !== $ibmd)
		{
			$mdd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $ibmd->contents);
			if(false !== $mdd && $mdd->getTimestamp() > $date->getTimestamp())
			{
				return 0;
			}
		}
		
		$this->collectIgnAddressIds($endpoint, $date, $departementId);
		
		$mdcid = 'insee_ban_address.'.$departementId.'.'.$date->format('Y-m');
		$mdc = InseeBanMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeBanMetadata();
			$mdc->insee_ban_metadata_id = $mdcid;
			$mdc->contents = '0';
			$mdc->save();
		}
		
		$count = 0;
		$knownpc = new InseeBanKnownPostalCodes();
		
		foreach($endpoint->getBanLinesIterator($date, $departementId) as $k => $banLine)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Ban Address Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Ban Address Lines...', ['k' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$mdc->save();
			}
			
			$count += $this->updateBanLine($knownpc, $banLine);
		}
		
		if(null === $ibmd)
		{
			$ibmd = new InseeBanMetadata();
			$ibmd->insee_ban_metadata_id = 'insee_ban_address.'.$departementId;
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $ibmd->contents);
		if(empty($dti) || $dti->getTimestamp() < $date->getTimestamp())
		{
			$ibmd->contents = $date->format('Y-m-d');
			$ibmd->save();
		}
		
		return $count;
	}
	
	/**
	 * Collects the ign addresses ids.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param string $departementId
	 * @throws \yii\db\Exception
	 * @throws RuntimeException
	 */
	public function collectIgnAddressIds(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, string $departementId) : void
	{
		$this->_knownIgnAddressIds = [];
		
		/** @var ApiFrInseeBanIgnAddress $addressLine */
		foreach($endpoint->getBanIgnAddressesIterator($date, $departementId) as $addressLine)
		{
			$this->_knownIgnAddressIds[(string) $addressLine->getIdBanAdresse()] = (string) $addressLine->getIdIgnAdresse();
		}
	}
	
	/**
	 * Updates all the ban lines for the given ban line.
	 *
	 * @param InseeBanKnownPostalCodes $knownpc
	 * @param ApiFrInseeBanLineInterface $banLine
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateBanLine(InseeBanKnownPostalCodes $knownpc, ApiFrInseeBanLineInterface $banLine) : int
	{
		$saved = 0;
		$codePostal = $knownpc->getPostalCode($banLine);
		
		$id = $this->b64usFromHex(\str_replace('ban-housenumber-', '', (string) $banLine->getIdBanAdresse()));
		$fkey = $this->b64usFromHex(\str_replace('ban-group-', '', (string) $banLine->getIdBanGroup()).'0'.$codePostal);
		$ignAddressId = null;
		if(isset($this->_knownIgnAddressIds[(string) $banLine->getIdBanAdresse()]))
		{
			$ignAddressId = (int) \ltrim(\str_replace('ADRNIVX_', '', $this->_knownIgnAddressIds[(string) $banLine->getIdBanAdresse()]), '0');
		}
		if(0 === $ignAddressId)
		{
			$ignAddressId = null;
		}
		
// 		if(\is_numeric($banLine->getNumero()))
// 		{
			$numero = $banLine->getNumero();
			$suffixe = $banLine->getSuffixe() === null || $banLine->getSuffixe() === '' ? null : $banLine->getSuffixe();
// 		}
// 		else
// 		{
// 			$matches = [];
// 			if(\preg_match('#^(\d+)(.*)$#', $banLine->getNumero(), $matches))
// 			{
// 				$numero = $matches[1];
// 				$suffixe = \trim($matches[2]);
// 			}
// 			else
// 			{
// 				$numero = $banLine->getNumero();
// 				$suffixe = empty($banLine->getSuffixe()) ? null : $banLine->getSuffixe();
// 			}
// 		}
		
		$suffixe = $this->_slugifier->slugify($suffixe);
		
		$inseeBanAddress = InseeBanAddress::findOne($id);
		if(null === $inseeBanAddress)
		{
			$inseeBanAddress = new InseeBanAddress();
			$inseeBanAddress->insee_ban_address_id = $id;
			$inseeBanAddress->insee_ban_group_id = $fkey;
			$inseeBanAddress->date_last_update = $banLine->getDateDerMajHn()->format('Y-m-d');
			$inseeBanAddress->ign_address_id = $ignAddressId;
			$inseeBanAddress->interop_id = $this->_slugifier->slugify($banLine->getCleInterop());
			$inseeBanAddress->number = (int) $numero;
			$inseeBanAddress->suffixe = $suffixe;
			if(!$inseeBanAddress->save())
			{
				$errors = [];
				
				foreach((array) $inseeBanAddress->getErrorSummary(true) as $error)
				{
					$errors[] = (string) $error;
				}
				
				$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
				$context = [
					'{class}' => \get_class($inseeBanAddress),
					'{errs}' => \implode(',', $errors),
					'{obj}' => \json_encode($inseeBanAddress->getAttributes(), \JSON_PRETTY_PRINT),
					'{old}' => \json_encode($inseeBanAddress->getOldAttributes(), \JSON_PRETTY_PRINT),
				];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			$saved++;
		}
		else
		{
			$blu = DateTimeImmutable::createFromFormat('Y-m-d', $inseeBanAddress->date_last_update);
			if(false === $blu)
			{
				$message = 'Failed to parse date last update "{dt}"';
				$context = ['{dt}' => $inseeBanAddress->date_last_update];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			$llu = $banLine->getDateDerMajHn();
			$bludays = 12 * 32 * ((int) $blu->format('Y')) + 32 * ((int) $blu->format('m') - 1) + ((int) $blu->format('d'));
			$lludays = 12 * 32 * ((int) $llu->format('Y')) + 32 * ((int) $llu->format('m') - 1) + ((int) $llu->format('d'));
			$mustBeUpdated = $lludays > $bludays;
			
			$mustBeSaved = ((string) $inseeBanAddress->insee_ban_group_id) !== ((string) $fkey)
				|| ((string) $inseeBanAddress->ign_address_id) !== ((string) $ignAddressId)
				|| ((string) $this->_slugifier->slugify($inseeBanAddress->interop_id)) !== ((string) $this->_slugifier->slugify($banLine->getCleInterop()))
				|| ((string) $inseeBanAddress->number) !== ((string) $numero)
				|| ((string) $inseeBanAddress->suffixe) !== ((string) $suffixe);
			
			if($mustBeSaved)
			{
				$saved += (int) $this->saveObjectClass(InseeBanAddressHistory::class, [
					'insee_ban_address_id' => $inseeBanAddress->insee_ban_address_id,
					'date_end_validity' => $inseeBanAddress->date_last_update,
				], [
					'insee_ban_group_id' => $inseeBanAddress->insee_ban_group_id,
					'ign_address_id' => $inseeBanAddress->ign_address_id,
					'interop_id' => $inseeBanAddress->interop_id,
					'number' => $inseeBanAddress->number,
					'suffixe' => $this->_slugifier->slugify($inseeBanAddress->suffixe),
				])->isNewRecord;
				
				$inseeBanAddress->date_last_update = $banLine->getDateDerMajHn()->format('Y-m-d');
				$inseeBanAddress->insee_ban_group_id = $fkey;
				$inseeBanAddress->ign_address_id = $ignAddressId;
				$inseeBanAddress->interop_id = $this->_slugifier->slugify($banLine->getCleInterop());
				$inseeBanAddress->number = (int) $numero;
				$inseeBanAddress->suffixe = $suffixe;
				if(!$inseeBanAddress->save())
				{
					$errors = [];
					
					foreach((array) $inseeBanAddress->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($inseeBanAddress),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($inseeBanAddress->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($inseeBanAddress->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$saved++;
			}
			elseif($mustBeUpdated)
			{
				$inseeBanAddress->date_last_update = $banLine->getDateDerMajHn()->format('Y-m-d');
				if(!$inseeBanAddress->save())
				{
					$errors = [];
					
					foreach((array) $inseeBanAddress->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($inseeBanAddress),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($inseeBanAddress->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($inseeBanAddress->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$saved++;
			}
		}
		
		return $saved;
	}
	
	/**
	 * Gets the base64 urlsafe encoded string of the given hexadecimal string.
	 * The hexadecimal string must have an odd number of hexadecimal digits.
	 *
	 * @param string $hexa
	 * @return string
	 */
	protected function b64usFromHex(string $hexa)
	{
		return \str_replace(['+', '/'], ['-', '_'], \trim(\base64_encode((string) \hex2bin($hexa)), '='));
	}
	
}
