<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLineInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeBan\Models\InseeBanMetadata;
use Yii2Module\Yii2InseeBan\Models\InseeBanPosition;
use Yii2Module\Yii2InseeBan\Models\InseeBanPositionHistory;

/**
 * InseeBanPositionUpdater class file.
 * 
 * This class updates the InseeBanPosition and InseeBanPositionHistory.
 * 
 * @author Anastaszor
 */
class InseeBanPositionUpdater extends ObjectUpdater
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new updater with the given logger.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Updates all the ban positions for all departements and all dates.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAll(ApiFrInseeBanEndpointInterface $endpoint, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateAllDate($endpoint, $date, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban positions for the given department and all dates.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDepartements(ApiFrInseeBanEndpointInterface $endpoint, string $departementId, bool $force = false) : int
	{
		$count = 0;
		
		foreach($endpoint->getUploadDates() as $date)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban positions for the given date and all departements.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateAllDate(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, bool $force = false) : int
	{
		$now = new DateTimeImmutable();
		if($date->format('Y-m-d') === $now->format('Y-m-d'))
		{
			$date = $endpoint->getLatestDate();
		}
		
		$count = 0;
		
		foreach($endpoint->getDepartementIds($date) as $departementId)
		{
			$count += $this->updateDepartement($endpoint, $date, $departementId, $force);
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban positions for the given date and departement.
	 *
	 * @param ApiFrInseeBanEndpointInterface $endpoint
	 * @param DateTimeInterface $date
	 * @param string $departementId
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateDepartement(ApiFrInseeBanEndpointInterface $endpoint, DateTimeInterface $date, string $departementId, bool $force = false) : int
	{
		$this->_logger->info(
			'Processing Ban Positions from Departement {did} at date {date}',
			['did' => $departementId, 'date' => $date->format('Y-m-d')],
		);
		
		$ibmd = InseeBanMetadata::findOne('insee_ban_position.'.$departementId);
		if(!$force && null !== $ibmd)
		{
			$mdd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $ibmd->contents);
			if(false !== $mdd && $mdd->getTimestamp() > $date->getTimestamp())
			{
				return 0;
			}
		}
		
		$mdcid = 'insee_ban_position.'.$departementId.'.'.$date->format('Y-m');
		$mdc = InseeBanMetadata::findOne($mdcid);
		if(null === $mdc)
		{
			$mdc = new InseeBanMetadata();
			$mdc->insee_ban_metadata_id = $mdcid;
			$mdc->contents = '0';
			$mdc->save();
		}
		
		$count = 0;
		
		foreach($endpoint->getBanLinesIterator($date, $departementId) as $k => $banLine)
		{
			if(($k + 1) < ((int) $mdc->contents))
			{
				if(0 === (((int) $k + 1) % 10000))
				{
					$this->_logger->info('Skipped {k} Ban Position Lines...', ['k' => (int) $k + 1]);
				}
				continue;
			}
			
			if(0 === (((int) $k + 1) % 10000))
			{
				$this->_logger->info('Processed {k} Ban Position Lines...', ['k' => (int) $k + 1]);
				$mdc->contents = (string) ($k + 1);
				$mdc->save();
			}
			
			$count += $this->updateBanLine($banLine);
		}
		
		if(null === $ibmd)
		{
			$ibmd = new InseeBanMetadata();
			$ibmd->insee_ban_metadata_id = 'insee_ban_position.'.$departementId;
		}
		$dti = DateTimeImmutable::createFromFormat('Y-m-d', (string) $ibmd->contents);
		if(empty($dti) || $dti->getTimestamp() < $date->getTimestamp())
		{
			$ibmd->contents = $date->format('Y-m-d');
			$ibmd->save();
		}
		
		return $count;
	}
	
	/**
	 * Updates all the ban lines for the given ban line.
	 *
	 * @param ApiFrInseeBanLineInterface $banLine
	 * @return integer the error code, 0 if no error
	 * @throws \yii\db\Exception
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function updateBanLine(ApiFrInseeBanLineInterface $banLine) : int
	{
		$saved = 0;
		$id = $this->b64usFromHex(\str_replace('ban-position-', '', (string) $banLine->getIdBanPosition()));
		$fkey = $this->b64usFromHex(\str_replace('ban-housenumber-', '', (string) $banLine->getIdBanAdresse()));
		$xCo = (int) ((float) $banLine->getX() * 100.0);
		$yCo = (int) ((float) $banLine->getY() * 100.0);
		$lat = (int) ((float) $banLine->getLat() * 1000000.0);
		$lon = (int) ((float) $banLine->getLon() * 1000000.0);
		$inseeBanPosition = InseeBanPosition::findOne($id);
		if(null === $inseeBanPosition)
		{
			$inseeBanPosition = new InseeBanPosition();
			$inseeBanPosition->insee_ban_position_id = $id;
			$inseeBanPosition->insee_ban_address_id = $fkey;
			$inseeBanPosition->date_last_update = $banLine->getDateDerMajPos()->format('Y-m-d');
			$inseeBanPosition->insee_ban_localisation_id = (int) $banLine->getTypLoc()->getId();
			$inseeBanPosition->insee_ban_source_id = (int) $banLine->getSource()->getId();
			$inseeBanPosition->x = $xCo;
			$inseeBanPosition->y = $yCo;
			$inseeBanPosition->latitude = $lat;
			$inseeBanPosition->longitude = $lon;
			if(!$inseeBanPosition->save())
			{
				$errors = [];
				
				foreach((array) $inseeBanPosition->getErrorSummary(true) as $error)
				{
					$errors[] = (string) $error;
				}
				
				$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
				$context = [
					'{class}' => \get_class($inseeBanPosition),
					'{errs}' => \implode(',', $errors),
					'{obj}' => \json_encode($inseeBanPosition->getAttributes(), \JSON_PRETTY_PRINT),
					'{old}' => \json_encode($inseeBanPosition->getOldAttributes(), \JSON_PRETTY_PRINT),
				];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			$saved++;
		}
		else
		{
			$blu = DateTimeImmutable::createFromFormat('Y-m-d', $inseeBanPosition->date_last_update);
			if(false === $blu)
			{
				$message = 'Failed to parse date last update "{dt}"';
				$context = ['{dt}' => $inseeBanPosition->date_last_update];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			$llu = $banLine->getDateDerMajPos();
			$bludays = 12 * 32 * ((int) $blu->format('Y')) + 32 * ((int) $blu->format('m') - 1) + ((int) $blu->format('d'));
			$lludays = 12 * 32 * ((int) $llu->format('Y')) + 32 * ((int) $llu->format('m') - 1) + ((int) $llu->format('d'));
			$mustBeUpdated = $lludays > $bludays;
			
			$mustBeSaved = (string) $inseeBanPosition->insee_ban_address_id !== (string) $fkey
				|| (string) $inseeBanPosition->insee_ban_localisation_id !== (string) $banLine->getTypLoc()->getId()
				|| (string) $inseeBanPosition->insee_ban_source_id !== (string) $banLine->getSource()->getId()
				|| (string) $inseeBanPosition->x !== (string) $xCo
				|| (string) $inseeBanPosition->y !== (string) $yCo
				|| (string) $inseeBanPosition->latitude !== (string) $lat
				|| (string) $inseeBanPosition->longitude !== (string) $lon;
			
			if($mustBeSaved)
			{
				$saved += (int) $this->saveObjectClass(InseeBanPositionHistory::class, [
					'insee_ban_position_id' => $inseeBanPosition->insee_ban_position_id,
					'date_end_validity' => $inseeBanPosition->date_last_update,
				], [
					'insee_ban_address_id' => $inseeBanPosition->insee_ban_address_id,
					'insee_ban_localisation_id' => $inseeBanPosition->insee_ban_localisation_id,
					'insee_ban_source_id' => $inseeBanPosition->insee_ban_source_id,
					'x' => $inseeBanPosition->x,
					'y' => $inseeBanPosition->y,
					'latitude' => $inseeBanPosition->latitude,
					'longitude' => $inseeBanPosition->longitude,
				])->isNewRecord;
				
				$inseeBanPosition->date_last_update = $banLine->getDateDerMajPos()->format('Y-m-d');
				$inseeBanPosition->insee_ban_address_id = $fkey;
				$inseeBanPosition->insee_ban_localisation_id = (int) $banLine->getTypLoc()->getId();
				$inseeBanPosition->insee_ban_source_id = (int) $banLine->getSource()->getId();
				$inseeBanPosition->x = $xCo;
				$inseeBanPosition->y = $yCo;
				$inseeBanPosition->latitude = $lat;
				$inseeBanPosition->longitude = $lon;
				if(!$inseeBanPosition->save())
				{
					$errors = [];
					
					foreach((array) $inseeBanPosition->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($inseeBanPosition),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($inseeBanPosition->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($inseeBanPosition->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$saved++;
			}
			elseif($mustBeUpdated)
			{
				$inseeBanPosition->date_last_update = $banLine->getDateDerMajPos()->format('Y-m-d');
				if(!$inseeBanPosition->save())
				{
					$errors = [];
					
					foreach((array) $inseeBanPosition->getErrorSummary(true) as $error)
					{
						$errors[] = (string) $error;
					}
					
					$message = "Failed to save {class} : {errs}\n{obj}\n{old}";
					$context = [
						'{class}' => \get_class($inseeBanPosition),
						'{errs}' => \implode(',', $errors),
						'{obj}' => \json_encode($inseeBanPosition->getAttributes(), \JSON_PRETTY_PRINT),
						'{old}' => \json_encode($inseeBanPosition->getOldAttributes(), \JSON_PRETTY_PRINT),
					];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$saved++;
			}
		}
		
		return $saved;
	}
	
	/**
	 * Gets the base64 urlsafe encoded string of the given hexadecimal string.
	 * The hexadecimal string must have an odd number of hexadecimal digits.
	 *
	 * @param string $hexa
	 * @return string
	 */
	protected function b64usFromHex(string $hexa)
	{
		return \str_replace(['+', '/'], ['-', '_'], \trim(\base64_encode((string) \hex2bin($hexa)), '='));
	}
	
}
