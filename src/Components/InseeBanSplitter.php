<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoie;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoieInterface;
use PhpExtended\Slugifier\SlugifierFactoryInterface;
use PhpExtended\Slugifier\SlugifierInterface;
use PhpExtended\Slugifier\SlugifierOptions;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Stringable;

/**
 * InseeBanSplitter class file.
 * 
 * This class is a parser that extracts normalized type voie from the nom voie.
 * 
 * @author Anastaszor
 */
class InseeBanSplitter implements Stringable
{
	
	/**
	 * The aliases of existing types [alias => abbrev].
	 * 
	 * @var array<string, string>
	 */
	protected array $_aliases = [
		'CI' => 'ZI',
		'CHEM' => 'CHE',
		'CHEM1' => 'CHE',
		'LDT' => 'LD',
		'R' => 'RUE',
		'VOIE1' => 'VOIE',
		'VLGE' => 'VGE',
	];
	
	/**
	 * The existing type voies.
	 * 
	 * @var array<string, ApiFrInseeBanTypeVoieInterface>
	 */
	protected array $_typeVoies = [];
	
	/**
	 * The slugifier.
	 * 
	 * @var SlugifierInterface
	 */
	protected SlugifierInterface $_slugifier;
	
	/**
	 * The logger.
	 * 
	 * @var ?LoggerInterface
	 */
	protected ?LoggerInterface $_logger = null;
	
	/**
	 * Builds a new InseeBanSplitter with the given endpoint.
	 * 
	 * @param array<integer|string, ApiFrInseeBanTypeVoieInterface> $typeVoies
	 * @param SlugifierFactoryInterface $slugifierFactory
	 * @param ?LoggerInterface $logger
	 */
	public function __construct(array $typeVoies, SlugifierFactoryInterface $slugifierFactory, ?LoggerInterface $logger = null)
	{
		/** @var ApiFrInseeBanTypeVoieInterface $typeVoie */
		foreach($typeVoies as $typeVoie)
		{
			$this->_typeVoies[(string) $typeVoie->getCode()] = $typeVoie;
		}
		
		// double chaining for all abbreviations used for types
		foreach($this->_aliases as $alias => $abbrev)
		{
			if(isset($this->_typeVoies[$abbrev]))
			{
				$this->_typeVoies[$alias] = $this->_typeVoies[$abbrev];
			}
		}
		
		$slugifierOptions = new SlugifierOptions();
		$slugifierOptions->setSeparator('-');
		$this->_slugifier = $slugifierFactory->createSlugifier($slugifierOptions);
		$this->_logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Logs the given debug message.
	 * 
	 * @param string $message
	 * @param array<string, null|boolean|integer|float|string> $context
	 */
	public function debug(string $message, array $context = []) : void
	{
		if(null !== $this->_logger)
		{
			$this->_logger->debug($message, $context);
		}
	}
	
	/**
	 * Gets the split between the nom voie and the normalized type voie.
	 * 
	 * @param string $nomVoie
	 * @return InseeBanSplittedVoie
	 * @throws RuntimeException if the parsing fails
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function split(?string $nomVoie) : InseeBanSplittedVoie
	{
		$this->debug('SPLITTER : Processing {nvoie}', [
			'nvoie' => null === $nomVoie ? '(null)' : $nomVoie,
		]);
		
		// {{{ Phase 0 : prepare the split with removing existing spaces
		// 				 for example transform "Lieu Dit" in "lieu-dit"
		/** @var ApiFrInseeBanTypeVoie $typeVoie */
		foreach($this->_typeVoies as $typeVoie)
		{
			if(\mb_strpos((string) $typeVoie->getName(), '-') !== false || \mb_strpos((string) $typeVoie->getName(), ' ') !== false)
			{
				$old = \str_ireplace('-', ' ', (string) $typeVoie->getName());
				$new = \str_ireplace(' ', '-', (string) $typeVoie->getName());
				$nomVoie = \str_ireplace($old, $new, (string) $nomVoie);
			}
		}
		// }}} End Phase 0
		
		$nomVoieArr = \explode(' ', \trim((string) $nomVoie));
		
		// if there is only one word, lets make this a Lieu-Dit
		if(\count($nomVoieArr) === 1 && isset($this->_typeVoies['LD']))
		{
			return new InseeBanSplittedVoie($this->_typeVoies['LD'], \implode(' ', $nomVoieArr));
		}
		
		$prefix = $nomVoieArr[0];
		$lPrefix = $this->_slugifier->slugify($prefix);
		unset($nomVoieArr[0]);
		$remaining = \implode(' ', $nomVoieArr);
		
		// {{{ Phase 1 : if type is written as is, use it
		/** @var ApiFrInseeBanTypeVoie $typeVoie */
		foreach($this->_typeVoies as $typeVoie)
		{
			$lTypeVoie = $this->_slugifier->slugify($typeVoie->getName());
			if($lTypeVoie === $lPrefix)
			{
				$this->debug('SPLITTER : Found {type} {desc}', [
					'type' => $typeVoie->getCode(),
					'desc' => $typeVoie->getName(),
				]);
				
				return new InseeBanSplittedVoie($typeVoie, $remaining);
			}
		}
		// }}} End Phase 1
		
		// {{{ Phase 2 : if type is written as abbrev, use it
		/** @var ApiFrInseeBanTypeVoie $typeVoie */
		foreach($this->_typeVoies as $typeVoie)
		{
			$lTypeVoie = $this->_slugifier->slugify($typeVoie->getCode());
			if($lTypeVoie === $lPrefix)
			{
				$this->debug('SPLITTER : Found {type} {desc}', [
					'type' => $typeVoie->getCode(),
					'desc' => $typeVoie->getName(),
				]);
				
				return new InseeBanSplittedVoie($typeVoie, $remaining);
			}
		}
		// }}} End Phase 2
		
		// {{{ Phase 3 : handle specific prefies
		if(isset($this->_typeVoies['RUE']))
		{
			// removable prefixes
			foreach([
				'r' => $this->_typeVoies['RUE'], // rue
			] as $nPrefix => $typeVoie)
			{
				if(\trim($lPrefix, 's') === $nPrefix)
				{
					$this->debug('SPLITTER : Found {type} {desc}', [
						'type' => $typeVoie->getCode(),
						'desc' => $typeVoie->getName(),
					]);
					
					return new InseeBanSplittedVoie($typeVoie, $remaining);
				}
			}
		}
		// }}} End Phase 3
		
		if(isset($this->_typeVoies['LD']))
		{
			return new InseeBanSplittedVoie($this->_typeVoies['LD'], $prefix.' '.$remaining);
		}
		
		$message = 'Failed to find a suitable type for nom voie : "{nom}"';
		$context = ['{nom}' => $nomVoie];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
}
