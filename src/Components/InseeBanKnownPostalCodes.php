<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Components;

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLineInterface;

/**
 * InseeBanKnownPostalCodes class file.
 * 
 * This class registers the postal codes to be able to restitute them.
 * 
 * @author Anastaszor
 */
class InseeBanKnownPostalCodes
{
	
	/**
	 * Gets the known postal codes for missing ones.
	 * [string:insee_id [string:group_hash => string:postal_code]].
	 *
	 * @var array<string, array<string, string>>
	 */
	protected array $_knownPostalCodes = [];
	
	/**
	 * Gets the postal code from the ban line.
	 * 
	 * @param ApiFrInseeBanLineInterface $banLine
	 * @return string
	 */
	public function getPostalCode(ApiFrInseeBanLineInterface $banLine) : string
	{
		$codePostal = (string) $banLine->getCodePostal();
		$codeInsee = (string) $banLine->getCodeInsee();
		$idBanGroup = (string) $banLine->getIdBanGroup();
		
		if(empty($codePostal))
		{
			$codePostal = $codeInsee;
			
			if(isset($this->_knownPostalCodes[$codeInsee][$idBanGroup]))
			{
				$codePostal = $this->_knownPostalCodes[$codeInsee][$idBanGroup];
			}
			
			return $codePostal;
		}
		
		return $this->_knownPostalCodes[$codeInsee][$idBanGroup] = $codePostal;
	}
	
}
