<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Commands;

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpoint;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoie;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpClient\ClientFactory;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Slugifier\SlugifierFactory;
use RuntimeException;
use yii\base\InvalidArgumentException;
use yii\BaseYii;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2InseeBan\Components\InseeBanGroupUpdater;
use Yii2Module\Yii2InseeBan\Components\InseeBanSplitter;
use Yii2Module\Yii2InseeBan\InseeBanModule;
use Yii2Module\Yii2InseeBan\Models\InseeBanTypeVoie;

/**
 * AlignController class file.
 * 
 * This commands updates the type voie parts from when they were not there.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class AlignController extends ExtendedController
{
	
	/**
	 * Does all the actions.
	 * 
	 * @return integer error code
	 */
	public function actionAll() : int
	{
		$ret = $this->actionBanGroup();
		
		return $ret + $this->actionBanGroupHistory();
	}
	
	/**
	 * Does the binding of the type voie with the ban groups.
	 * 
	 * @param boolean $force
	 * @return integer error code
	 */
	public function actionBanGroup($force = false) : int
	{
		return $this->runCallable(function() use ($force) : int
		{
			/** @var array<integer, InseeBanTypeVoie> $typeVoies */
			$typeVoies = InseeBanTypeVoie::find()->all();
			$splitter = new InseeBanSplitter($this->transformTypeVoies($typeVoies), new SlugifierFactory());
			$updater = new InseeBanGroupUpdater($this->getLogger(), $splitter);
			$updater->realignGroupWithTypeVoie($force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Does the binding of the type voie with the ban group histories.
	 * 
	 * @param boolean $force
	 * @return integer error code
	 */
	public function actionBanGroupHistory($force = false) : int
	{
		return $this->runCallable(function() use ($force) : int
		{
			/** @var array<integer, InseeBanTypeVoie> $typeVoies */
			$typeVoies = InseeBanTypeVoie::find()->all();
			$splitter = new InseeBanSplitter($this->transformTypeVoies($typeVoies), new SlugifierFactory());
			$updater = new InseeBanGroupUpdater($this->getLogger(), $splitter);
			$updater->realignGroupHistoryWithTypeVoie((bool) $force);

			return ExitCode::OK;
		});
	}
	
	/**
	 * @param array<integer, InseeBanTypeVoie> $typeVoies
	 * @return array<integer, ApiFrInseeBanTypeVoie>
	 */
	protected function transformTypeVoies(array $typeVoies) : array
	{
		$return = [];
		
		/** @var InseeBanTypeVoie $typeVoie */
		foreach($typeVoies as $typeVoie)
		{
			$new = new ApiFrInseeBanTypeVoie(
				$typeVoie->insee_ban_type_voie_id,
				$typeVoie->code,
				$typeVoie->description,
			);
			$return[] = $new;
		}
		
		return $return;
	}
	
	/**
	 * Gets the ban endpoint with its dependancies.
	 *
	 * @return ApiFrInseeBanEndpoint
	 * @throws \InvalidArgumentException
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	protected function getBanEndpoint() : ApiFrInseeBanEndpoint
	{
		$tempDirPath = (string) BaseYii::getAlias('@app/runtime/'.InseeBanModule::getInstance()->id);
		$clientFactory = new ClientFactory();
		$clientFactory->setLogger($this->getLogger());
		$clientFactory->getConfiguration()->disablePreferCurl();
		$client = $clientFactory->createClient();
		
		return new ApiFrInseeBanEndpoint($tempDirPath, $client);
	}
	
}
