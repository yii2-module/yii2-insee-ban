<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan\Commands;

use DateTimeImmutable;
use DateTimeInterface;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpoint;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoie;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpClient\ClientFactory;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Slugifier\SlugifierFactory;
use RuntimeException;
use yii\base\InvalidArgumentException;
use yii\BaseYii;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2InseeBan\Components\InseeBanAddressUpdater;
use Yii2Module\Yii2InseeBan\Components\InseeBanAllUpdater;
use Yii2Module\Yii2InseeBan\Components\InseeBanBulkUpdater;
use Yii2Module\Yii2InseeBan\Components\InseeBanGroupUpdater;
use Yii2Module\Yii2InseeBan\Components\InseeBanLocalisationUpdater;
use Yii2Module\Yii2InseeBan\Components\InseeBanPositionUpdater;
use Yii2Module\Yii2InseeBan\Components\InseeBanPostalAreaUpdater;
use Yii2Module\Yii2InseeBan\Components\InseeBanSourceUpdater;
use Yii2Module\Yii2InseeBan\Components\InseeBanSplitter;
use Yii2Module\Yii2InseeBan\Components\InseeBanTypeVoieUpdater;
use Yii2Module\Yii2InseeBan\InseeBanModule;
use Yii2Module\Yii2InseeBan\Models\InseeBanTypeVoie;

/**
 * UpdateController class file.
 * 
 * This command updates all parts of the insee ban database.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class UpdateController extends ExtendedController
{
	
	/**
	 * Updates all the records (postal areas, groups, addresses and positions)
	 * for the givne date and departements. If the date is not given, all dates
	 * will be used, from oldest to earliest. If the departement is not given,
	 * all departements will be used. If the date is the current date, then only
	 * the latest date will be used.
	 * 
	 * @param boolean $bulk
	 * @param string $date
	 * @param string $departement
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function actionAll($bulk = true, $date = null, $departement = null, $force = false) : int
	{
		if($bulk)
		{
			return $this->actionBulk($departement);
		}
		
		\ini_set('memory_limit', '256M');
		
		return $this->runCallable(function() use ($date, $departement, $force) : int
		{
			$dymd = null;
			if(null !== $date)
			{
				$dymd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $date);
				if(false === $dymd)
				{
					$message = 'Failed to parse "{data}" into datetime with format Y-m-d';
					$context = ['{data}' => (string) $date];
					
					throw new RuntimeException(\strtr($message, $context));
				}
			}
			$logger = $this->getLogger();
			$endpoint = $this->getBanEndpoint();
			/** @var array<integer, InseeBanTypeVoie> $typeVoies */
			$typeVoies = InseeBanTypeVoie::find()->all();
			$splitter = new InseeBanSplitter($this->transformTypeVoies($typeVoies), new SlugifierFactory());
			$updater = new InseeBanSourceUpdater($logger);
			$updater->updateAll($endpoint, (bool) $force);
			$updater = new InseeBanLocalisationUpdater($logger);
			$updater->updateAll($endpoint, (bool) $force);
			$updater = new InseeBanAllUpdater($logger, new InseeBanPostalAreaUpdater($logger), new InseeBanGroupUpdater($logger, $splitter), new InseeBanAddressUpdater($logger), new InseeBanPositionUpdater($logger));
			if(null === $dymd && null === $departement)
			{
				$updater->updateAll($endpoint, (bool) $force);
			}
			elseif(null === $dymd)
			{
				$updater->updateAllDepartements($endpoint, (string) $departement, (bool) $force);
			}
			elseif(null === $departement)
			{
				$updater->updateAllDate($endpoint, $dymd, (bool) $force);
			}
			else
			{
				$updater->updateDepartement($endpoint, $dymd, (string) $departement, (bool) $force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates all the records (postal areas, groups, addresses and positions)
	 * for the given date and departements. All dates will be processed from
	 * oldest to newest. If the departements is not given, all departements
	 * will be processed.
	 *
	 * @param string $departement
	 * @return integer the error code, 0 if no error
	 */
	public function actionBulk($departement = null) : int
	{
		\ini_set('memory_limit', '5G');
		
		return $this->runCallable(function() use ($departement) : int
		{
			$logger = $this->getLogger();
			$endpoint = $this->getBanEndpoint();
			/** @var array<integer, InseeBanTypeVoie> $typeVoies */
			$typeVoies = InseeBanTypeVoie::find()->all();
			$splitter = new InseeBanSplitter($this->transformTypeVoies($typeVoies), new SlugifierFactory());
			$slugifier = (new SlugifierFactory())->createSlugifier();
			$updater = new InseeBanBulkUpdater($logger, $endpoint, $slugifier, $splitter);
			null === $departement
				? $updater->updateAllDepartements()
				: $updater->updateDepartement($departement);

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the postal areas for the given date and departements. If the
	 * date is not given, all dates will be used, from oldest to earliest. If
	 * the departement is not given, all departements will be used. If date is
	 * the current date, then only the latest date will be used.
	 * 
	 * @param string $date
	 * @param string $departement
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function actionPostalArea($date = null, $departement = null, $force = false) : int
	{
		return $this->runCallable(function() use ($date, $departement, $force) : int
		{
			$dymd = null;
			if(null === $date)
			{
				$dymd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $date);
				if(false === $dymd)
				{
					$message = 'Failed to parse "{data}" into datetime with format Y-m-d';
					$context = ['{data}' => (string) $date];
					
					throw new RuntimeException(\strtr($message, $context));
				}
			}
			$updater = new InseeBanPostalAreaUpdater($this->getLogger());
			$endpoint = $this->getBanEndpoint();
			if(null === $dymd && null === $departement)
			{
				$updater->updateAll($endpoint, (bool) $force);
			}
			elseif(null === $dymd)
			{
				$updater->updateAllDepartements($endpoint, (string) $departement, (bool) $force);
			}
			elseif(null === $departement)
			{
				$updater->updateAllDate($endpoint, $dymd, (bool) $force);
			}
			else
			{
				$updater->updateDepartement($endpoint, $dymd, (string) $departement, (bool) $force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the groups for the given date and departements. If the date
	 * is not given, all dates will be used, from oldest to earliest. If
	 * the departement is not given, all departements will be used. If date is
	 * the current date, then only the latest date will be used.
	 * 
	 * @param string $date
	 * @param string $departement
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function actionGroup($date = null, $departement = null, $force = false) : int
	{
		return $this->runCallable(function() use ($date, $departement, $force) : int
		{
			$dymd = null;
			if(null === $date)
			{
				$dymd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $date);
				if(false === $dymd)
				{
					$message = 'Failed to parse "{data}" into datetime with format Y-m-d';
					$context = ['{data}' => (string) $date];
					
					throw new RuntimeException(\strtr($message, $context));
				}
			}
			/** @var array<integer, InseeBanTypeVoie> $typeVoies */
			$typeVoies = InseeBanTypeVoie::find()->all();
			$splitter = new InseeBanSplitter($this->transformTypeVoies($typeVoies), new SlugifierFactory());
			$updater = new InseeBanGroupUpdater($this->getLogger(), $splitter);
			$endpoint = $this->getBanEndpoint();
			if(null === $dymd && null === $departement)
			{
				$updater->updateAll($endpoint, (bool) $force);
			}
			elseif(null === $dymd)
			{
				$updater->updateAllDepartements($endpoint, (string) $departement, (bool) $force);
			}
			elseif(null === $departement)
			{
				$updater->updateAllDate($endpoint, $dymd, (bool) $force);
			}
			else
			{
				$updater->updateDepartement($endpoint, $dymd, (string) $departement, (bool) $force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the addresses for the given date and departements. If the date
	 * is not given, all dates will be used, from oldest to earliest. If
	 * the departement is not given, all departements will be used. If date is
	 * the current date, then only the latest date will be used.
	 * 
	 * @param string $date
	 * @param string $departement
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function actionAddress($date = null, $departement = null, $force = false) : int
	{
		\ini_set('memory_limit', '256M');
		
		return $this->runCallable(function() use ($date, $departement, $force) : int
		{
			$dymd = null;
			if(null === $date)
			{
				$dymd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $date);
				if(false === $dymd)
				{
					$message = 'Failed to parse "{data}" into datetime with format Y-m-d';
					$context = ['{data}' => (string) $date];
					
					throw new RuntimeException(\strtr($message, $context));
				}
			}
			$updater = new InseeBanAddressUpdater($this->getLogger());
			$endpoint = $this->getBanEndpoint();
			if(null === $dymd && null === $departement)
			{
				$updater->updateAll($endpoint, (bool) $force);
			}
			elseif(null === $dymd)
			{
				$updater->updateAllDepartements($endpoint, (string) $departement, (bool) $force);
			}
			elseif(null === $departement)
			{
				$updater->updateAllDate($endpoint, $dymd, (bool) $force);
			}
			else
			{
				$updater->updateDepartement($endpoint, $dymd, (string) $departement, (bool) $force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the positions for the given date and departements. If the date 
	 * is not given, all dates will be used, from oldest to earliest. If the
	 * departement is not given, all departements will be used. If date is the
	 * current date, then only the latest date will be used.
	 * 
	 * @param string $date
	 * @param string $departement
	 * @param boolean $force if true, force regenerate of all records
	 * @return integer the error code, 0 if no error
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function actionPosition($date = null, $departement = null, $force = false) : int
	{
		return $this->runCallable(function() use ($date, $departement, $force) : int
		{
			$dymd = null;
			if(null === $date)
			{
				$dymd = DateTimeImmutable::createFromFormat('Y-m-d', (string) $date);
				if(false === $dymd)
				{
					$message = 'Failed to parse "{data}" into datetime with format Y-m-d';
					$context = ['{data}' => (string) $date];
					
					throw new RuntimeException(\strtr($message, $context));
				}
			}
			$updater = new InseeBanPositionUpdater($this->getLogger());
			$endpoint = $this->getBanEndpoint();
			if(null === $dymd && null === $departement)
			{
				$updater->updateAll($endpoint, (bool) $force);
			}
			elseif(null === $dymd)
			{
				$updater->updateAllDepartements($endpoint, (string) $departement, (bool) $force);
			}
			elseif(null === $departement)
			{
				$updater->updateAllDate($endpoint, $dymd, (bool) $force);
			}
			else
			{
				$updater->updateDepartement($endpoint, $dymd, (string) $departement, (bool) $force);
			}

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the types voies from the current file given by the insee.
	 *
	 * @param ?DateTimeInterface $stopAt the time when reached we have to stop
	 * @return integer the error code, 0 if no error
	 */
	public function actionTypeVoie(?DateTimeInterface $stopAt = null) : int
	{
		return $this->runCallable(function() use ($stopAt) : int
		{
			$updater = new InseeBanTypeVoieUpdater($this->getLogger());
			$endpoint = $this->getBanEndpoint();
			$updater->updateAll($endpoint, $stopAt);

			return ExitCode::OK;
		});
	}
	
	/**
	 * @param array<integer, InseeBanTypeVoie> $typeVoies
	 * @return array<integer, ApiFrInseeBanTypeVoie>
	 */
	protected function transformTypeVoies(array $typeVoies) : array
	{
		$return = [];
		
		/** @var InseeBanTypeVoie $typeVoie */
		foreach($typeVoies as $typeVoie)
		{
			$new = new ApiFrInseeBanTypeVoie(
				$typeVoie->insee_ban_type_voie_id,
				$typeVoie->code,
				$typeVoie->description,
			);
			$return[] = $new;
		}
		
		return $return;
	}
	
	/**
	 * Gets the ban endpoint with its dependancies.
	 * 
	 * @return ApiFrInseeBanEndpoint
	 * @throws \InvalidArgumentException
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	protected function getBanEndpoint() : ApiFrInseeBanEndpoint
	{
		$tempDirPath = (string) BaseYii::getAlias('@app/runtime/'.InseeBanModule::getInstance()->id);
		$clientFactory = new ClientFactory();
		$clientFactory->setLogger($this->getLogger());
		$clientFactory->getConfiguration()->disablePreferCurl();
		$client = $clientFactory->createClient();
		
		return new ApiFrInseeBanEndpoint($tempDirPath, $client);
	}
	
}
