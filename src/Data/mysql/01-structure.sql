/**
 * Database structure required by InseeBanModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-ban
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `insee_ban_metadata`;
DROP TABLE IF EXISTS `insee_ban_type_voie`;
DROP TABLE IF EXISTS `insee_ban_localisation`;
DROP TABLE IF EXISTS `insee_ban_source`;
DROP TABLE IF EXISTS `insee_ban_postal_area`;
DROP TABLE IF EXISTS `insee_ban_postal_area_history`;
DROP TABLE IF EXISTS `insee_ban_group`;
DROP TABLE IF EXISTS `insee_ban_group_history`;
DROP TABLE IF EXISTS `insee_ban_address`;
DROP TABLE IF EXISTS `insee_ban_address_history`;
DROP TABLE IF EXISTS `insee_ban_position`;
DROP TABLE IF EXISTS `insee_ban_position_history`;

SET foreign_key_checks = 1;


CREATE TABLE `insee_ban_metadata`
(
	`insee_ban_metadata_id` VARCHAR(64) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The key',
	`contents` VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The value'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban metadata';

CREATE TABLE `insee_ban_type_voie`
(
	`insee_ban_type_voie_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the type voie',
	`code` VARCHAR(4) NOT NULL COLLATE ascii_bin COMMENT 'The code of the type voie',
	`description` VARCHAR(127) NOT NULL COLLATE utf8_general_ci COMMENT 'The description of the type voie'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene type voie';

CREATE TABLE `insee_ban_localisation`
(
	`insee_ban_localisation_id` TINYINT(1) NOT NULL PRIMARY KEY COMMENT 'The id of the ban localisation',
	`code` VARCHAR(64) NOT NULL COLLATE ascii_bin COMMENT 'The code of the ban localisation',
	`precision` TINYINT(1) NOT NULL COMMENT 'The precision of the localisation, lower is better',
	`description` VARCHAR(512) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The description of the ban localisation' 
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban localisations';

CREATE TABLE `insee_ban_source`
(
	`insee_ban_source_id` TINYINT(1) NOT NULL PRIMARY KEY COMMENT 'The id of the ban source',
	`code` VARCHAR(64) NOT NULL COLLATE ascii_bin COMMENT 'The code of the ban source',
	`description` VARCHAR(512) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The description of the ban source'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban sources';

CREATE TABLE `insee_ban_postal_area`
(
	`insee_ban_postal_area_id` CHAR(7) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the ban postal area',
	`insee_cog_commune_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related commune',
	`date_last_update` DATE NOT NULL COMMENT 'The date when this postal area was last updated',
	`postal_code` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The postal code of this postal area',
	`name` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The full name of the postal area'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban postal areas';

CREATE TABLE `insee_ban_postal_area_history`
(
	`insee_ban_postal_area_id` CHAR(7) NOT NULL COLLATE ascii_bin COMMENT 'The id of the ban related postal area',
	`date_end_validity` DATE NOT NULL COMMENT 'The date when this postal area was last valid',
	`insee_cog_commune_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related commune',
	`postal_code` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The postal code of this postal area',
	`name` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The full name of the postal area',
	PRIMARY KEY (`insee_ban_postal_area_id`, `date_end_validity`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban postal areas history';

CREATE TABLE `insee_ban_group`
(
	`insee_ban_group_id` CHAR(26) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the ban group',
	`insee_ban_postal_area_id` CHAR(7) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related ban postal area',
	`insee_ban_type_voie_id` INT(11) NOT NULL COMMENT 'The id of the related type voie',
	`date_last_update` DATE NOT NULL COMMENT 'The date when this postal area was last updated',
	`fantoir_id` CHAR(9) DEFAULT NULL COLLATE ascii_bin COMMENT 'The fantoir id of the group',
	`ign_group_id` CHAR(9) DEFAULT NULL COLLATE ascii_bin COMMENT 'The ign group id of the group',
	`way_name` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The name of the way for this group',
	`complement_name` VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The complement name of the way'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban groups';

CREATE TABLE `insee_ban_group_history`
(
	`insee_ban_group_id` CHAR(26) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related ban group',
	`date_end_validity` DATE NOT NULL COMMENT 'The date when this group was last valid',
	`insee_ban_postal_area_id` CHAR(7) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related ban postal area',
	`insee_ban_type_voie_id` INT(11) NOT NULL COMMENT 'The id of the related type voie',
	`fantoir_id` CHAR(9) DEFAULT NULL COLLATE ascii_bin COMMENT 'The fantoir id of the group',
	`ign_group_id` CHAR(9) DEFAULT NULL COLLATE ascii_bin COMMENT 'The ign group id of the group',
	`way_name` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The name of the way for this group',
	`complement_name` VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'The complement name of the way',
	PRIMARY KEY (`insee_ban_group_id`, `date_end_validity`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban postal groups history';

CREATE TABLE `insee_ban_address`
(
	`insee_ban_address_id` CHAR(22) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the ban address',
	`insee_ban_group_id` CHAR(26) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related ban group',
	`date_last_update` DATE NOT NULL COMMENT 'The date when this postal area was last updated',
	`ign_address_id` INT(11) UNSIGNED DEFAULT NULL COMMENT 'The ign address id of this address', 
	`interop_id` VARCHAR(36) DEFAULT NULL COLLATE ascii_bin COMMENT 'The cle of the addres',
	`number` INT(11) NOT NULL COMMENT 'The number of this address on the way',
	`suffixe` VARCHAR(20) DEFAULT NULL COLLATE ascii_bin COMMENT 'The repetition index'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban addresses';

CREATE TABLE `insee_ban_address_history`
(
	`insee_ban_address_id` CHAR(22) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related ban address',
	`date_end_validity` DATE NOT NULL COMMENT 'The date when this address was last valid',
	`insee_ban_group_id` CHAR(26) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related ban group',
	`ign_address_id` INT(11) UNSIGNED DEFAULT NULL COMMENT 'The ign address id of this address',
	`interop_id` VARCHAR(36) DEFAULT NULL COLLATE ascii_bin COMMENT 'The cle of the addres', 
	`number` INT(11) NOT NULL COMMENT 'The number of this address on the way',
	`suffixe` VARCHAR(20) DEFAULT NULL COLLATE ascii_bin COMMENT 'The repetition index',
	PRIMARY KEY (`insee_ban_address_id`, `date_end_validity`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban addresses history';

CREATE TABLE `insee_ban_position`
(
	`insee_ban_position_id` CHAR(22) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the ban position',
	`insee_ban_address_id` CHAR(22) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related ban position',
	`date_last_update` DATE NOT NULL COMMENT 'The date when this postal area was last updated',
	`insee_ban_localisation_id` TINYINT(1) NOT NULL COMMENT 'The id of the type of localisation',
	`insee_ban_source_id` TINYINT(1) NOT NULL COMMENT 'The id of the source of the localisation',
	`x` INT(11) NOT NULL COMMENT 'The x coordinate of lambert-93 projection, x100',
	`y` INT(11) NOT NULL COMMENT 'The y coordinate of lambert-93 projection, x100',
	`latitude` INT(11) NOT NULL COMMENT 'The latitude wgs-84, x1.000.000',
	`longitude` INT(11) NOT NULL COMMENT 'The longitude wgs-84, x1.000.000'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban positions';

CREATE TABLE `insee_ban_position_history`
(
	`insee_ban_position_id` CHAR(22) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related ban position',
	`date_end_validity` DATE NOT NULL COMMENT 'The date when this position was last valid',
	`insee_ban_address_id` CHAR(22) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related ban position',
	`insee_ban_localisation_id` TINYINT(1) NOT NULL COMMENT 'The id of the type of localisation',
	`insee_ban_source_id` TINYINT(1) NOT NULL COMMENT 'The id of the source of the localisation',
	`x` INT(11) NOT NULL COMMENT 'The x coordinate of lambert-93 projection, x100',
	`y` INT(11) NOT NULL COMMENT 'The y coordinate of lambert-93 projection, x100',
	`latitude` INT(11) NOT NULL COMMENT 'The latitude wgs-84, x1.000.000',
	`longitude` INT(11) NOT NULL COMMENT 'The longitude wgs-84, x1.000.000',
	PRIMARY KEY (`insee_ban_position_id`, `date_end_validity`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the ban positions history';
