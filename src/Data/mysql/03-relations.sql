/**
 * Database relations required by InseeBanModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-ban
 * @license MIT
 */

ALTER TABLE `insee_ban_address` ADD CONSTRAINT `fk_insee_ban_address_insee_ban_group` FOREIGN KEY (`insee_ban_group_id`) REFERENCES `insee_ban_group`(`insee_ban_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_ban_address_history` ADD CONSTRAINT `fk_insee_ban_address_history_insee_ban_address` FOREIGN KEY (`insee_ban_address_id`) REFERENCES `insee_ban_address`(`insee_ban_address_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_address_history` ADD CONSTRAINT `fk_insee_ban_address_history_insee_ban_group` FOREIGN KEY (`insee_ban_group_id`) REFERENCES `insee_ban_group`(`insee_ban_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_ban_group` ADD CONSTRAINT `fk_insee_ban_group_insee_ban_postal_area` FOREIGN KEY (`insee_ban_postal_area_id`) REFERENCES `insee_ban_postal_area`(`insee_ban_postal_area_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_group` ADD CONSTRAINT `fk_insee_ban_group_insee_ban_type_voie` FOREIGN KEY (`insee_ban_type_voie_id`) REFERENCES `insee_ban_type_voie`(`insee_ban_type_voie_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_ban_group_history` ADD CONSTRAINT `fk_insee_ban_group_history_insee_ban_group` FOREIGN KEY (`insee_ban_group_id`) REFERENCES `insee_ban_group`(`insee_ban_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_group_history` ADD CONSTRAINT `fk_insee_ban_group_history_insee_ban_postal_area` FOREIGN KEY (`insee_ban_postal_area_id`) REFERENCES `insee_ban_postal_area`(`insee_ban_postal_area_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_group_history` ADD CONSTRAINT `fk_insee_ban_group_history_insee_ban_type_voie` FOREIGN KEY (`insee_ban_type_voie_id`) REFERENCES `insee_ban_type_voie`(`insee_ban_type_voie_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_ban_position` ADD CONSTRAINT `fk_insee_ban_position_insee_ban_address` FOREIGN KEY (`insee_ban_address_id`) REFERENCES `insee_ban_address`(`insee_ban_address_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_position` ADD CONSTRAINT `fk_insee_ban_position_insee_ban_localisation` FOREIGN KEY (`insee_ban_localisation_id`) REFERENCES `insee_ban_localisation`(`insee_ban_localisation_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_position` ADD CONSTRAINT `fk_insee_ban_position_insee_ban_source` FOREIGN KEY (`insee_ban_source_id`) REFERENCES `insee_ban_source`(`insee_ban_source_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_ban_position_history` ADD CONSTRAINT `fk_insee_ban_position_history_insee_ban_position` FOREIGN KEY (`insee_ban_position_id`) REFERENCES `insee_ban_position`(`insee_ban_position_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_position_history` ADD CONSTRAINT `fk_insee_ban_position_history_insee_ban_address` FOREIGN KEY (`insee_ban_address_id`) REFERENCES `insee_ban_address`(`insee_ban_address_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_position_history` ADD CONSTRAINT `fk_insee_ban_position_history_insee_ban_localisation` FOREIGN KEY (`insee_ban_localisation_id`) REFERENCES `insee_ban_localisation`(`insee_ban_localisation_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_position_history` ADD CONSTRAINT `fk_insee_ban_position_history_insee_ban_source` FOREIGN KEY (`insee_ban_source_id`) REFERENCES `insee_ban_source`(`insee_ban_source_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_ban_postal_area` ADD CONSTRAINT `fk_insee_ban_postal_area_insee_cog_commune` FOREIGN KEY (`insee_cog_commune_id`) REFERENCES `insee_cog_commune`(`insee_cog_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_ban_postal_area_history` ADD CONSTRAINT `fk_insee_ban_postal_area_history_insee_ban_postal_area` FOREIGN KEY (`insee_ban_postal_area_id`) REFERENCES `insee_ban_postal_area`(`insee_ban_postal_area_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `insee_ban_postal_area_history` ADD CONSTRAINT `fk_insee_ban_postal_area_history_insee_cog_commune` FOREIGN KEY (`insee_cog_commune_id`) REFERENCES `insee_cog_commune`(`insee_cog_commune_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

