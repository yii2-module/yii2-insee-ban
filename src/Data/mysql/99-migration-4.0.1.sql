/**
 * Database relations required by InseeBanModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-ban
 * @license MIT
 */

CREATE TABLE `insee_ban_type_voie`
(
	`insee_ban_type_voie_id` INT(11) NOT NULL PRIMARY KEY COMMENT 'The id of the type voie',
	`code` VARCHAR(4) NOT NULL COLLATE ascii_bin COMMENT 'The code of the type voie',
	`description` VARCHAR(127) NOT NULL COLLATE utf8_general_ci COMMENT 'The description of the type voie'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the sirene type voie';

ALTER TABLE `insee_ban_group`
ADD `insee_ban_type_voie_id` INT(11) DEFAULT NULL COMMENT 'The id of the related type voie'
AFTER `insee_ban_postal_area_id`;

ALTER TABLE `insee_ban_group_history`
ADD `insee_ban_type_voie_id` INT(11) DEFAULT NULL COMMENT 'The id of the related type voie'
AFTER `insee_ban_postal_area_id`;

ALTER TABLE `insee_ban_group`
ADD CONSTRAINT `fk_insee_ban_group_insee_ban_type_voie` FOREIGN KEY (`insee_ban_type_voie_id`) REFERENCES `insee_ban_type_voie`(`insee_ban_type_voie_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `insee_ban_group_history`
ADD CONSTRAINT `fk_insee_ban_group_history_insee_ban_type_voie` FOREIGN KEY (`insee_ban_type_voie_id`) REFERENCES `insee_ban_type_voie`(`insee_ban_type_voie_id`) ON DELETE RESTRICT ON UPDATE CASCADE;


