/**
 * Database relations required by InseeBanModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-ban
 * @license MIT
 */

ALTER TABLE `insee_ban_group`
MODIFY `insee_ban_type_voie_id` INT(11) NOT NULL COMMENT 'The id of the related type voie';

ALTER TABLE `insee_ban_group_history`
MODIFY `insee_ban_type_voie_id` INT(11) NOT NULL COMMENT 'The id of the related type voie';

DELETE FROM `insee_ban_position_history` WHERE 1;
DELETE FROM `insee_ban_address_history` WHERE 1;
DELETE FROM `insee_ban_group_history` WHERE 1;
DELETE FROM `insee_ban_postal_area_history` WHERE 1;
DELETE FROM `insee_ban_metadata` WHERE 1;
