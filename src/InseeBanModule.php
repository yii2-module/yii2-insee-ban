<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeBan;

use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2InseeBan\Components\InseeBanBayesianObjectFinder;
use Yii2Module\Yii2InseeBan\Components\InseeBanLevenshteinObjectFinder;
use Yii2Module\Yii2InseeBan\Models\InseeBanAddress;
use Yii2Module\Yii2InseeBan\Models\InseeBanAddressHistory;
use Yii2Module\Yii2InseeBan\Models\InseeBanGroup;
use Yii2Module\Yii2InseeBan\Models\InseeBanGroupHistory;
use Yii2Module\Yii2InseeBan\Models\InseeBanLocalisation;
use Yii2Module\Yii2InseeBan\Models\InseeBanMetadata;
use Yii2Module\Yii2InseeBan\Models\InseeBanPosition;
use Yii2Module\Yii2InseeBan\Models\InseeBanPositionHistory;
use Yii2Module\Yii2InseeBan\Models\InseeBanPostalArea;
use Yii2Module\Yii2InseeBan\Models\InseeBanSource;
use Yii2Module\Yii2InseeBan\Models\InseeBanTypeVoie;

/**
 * InseeBanModule class file.
 * 
 * This module is to represent the advanced geographic information about french
 * territory with consolidated informations, in order to be able to build other
 * resources that uses those standardized geographic features.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeBanModule extends BootstrappedModule
{
	
	/**
	 * The bayesian finder, if needed.
	 * 
	 * @var ?InseeBanBayesianObjectFinder
	 */
	protected ?InseeBanBayesianObjectFinder $_bayesianFinder = null;
	
	/**
	 * The levenshtein finder, if needed.
	 * 
	 * @var ?InseeBanLevenshteinObjectFinder
	 */
	protected ?InseeBanLevenshteinObjectFinder $_levenshteinFinder = null;
	
	/**
	 * Gets the object finder that uses bayesian probabilities.
	 * 
	 * @return InseeBanBayesianObjectFinder
	 */
	public function getBayesianObjectFinder() : InseeBanBayesianObjectFinder
	{
		if(null === $this->_bayesianFinder)
		{
			$this->_bayesianFinder = new InseeBanBayesianObjectFinder();
		}
		
		return $this->_bayesianFinder;
	}
	
	/**
	 * Gets the object finder that uses levenshtein distance.
	 * 
	 * @return InseeBanLevenshteinObjectFinder
	 */
	public function getLevenshteinObjectFinder() : InseeBanLevenshteinObjectFinder
	{
		if(null === $this->_levenshteinFinder)
		{
			$this->_levenshteinFinder = new InseeBanLevenshteinObjectFinder();
		}
		
		return $this->_levenshteinFinder;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'pin-map';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('InseeBanModule.Module', 'Base Adresse Nationale');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'ban' => new Bundle(BaseYii::t('InseeBanModule.Module', 'BAN'), [
				'address' => (new Record(InseeBanAddress::class, 'address', BaseYii::t('InseeBanModule.Module', 'Address')))->enableFullAccess(),
				'address-h' => (new Record(InseeBanAddressHistory::class, 'address-h', BaseYii::t('InseeBanModule.Module', 'Address History')))->enableFullAccess(),
				'group' => (new Record(InseeBanGroup::class, 'group', BaseYii::t('InseeBanModule.Module', 'Group')))->enableFullAccess(),
				'group-h' => (new Record(InseeBanGroupHistory::class, 'group-h', BaseYii::t('InseeBanModule.Module', 'Group History')))->enableFullAccess(),
				'localisation' => (new Record(InseeBanLocalisation::class, 'localisation', BaseYii::t('InseeBanModule.Module', 'Localisation')))->enableFullAccess(),
				'metadata' => (new Record(InseeBanMetadata::class, 'metadata', BaseYii::t('InseeBanModule.Module', 'Metadata')))->enableFullAccess(),
				'position' => (new Record(InseeBanPosition::class, 'position', BaseYii::t('InseeBanModule.Module', 'Position')))->enableFullAccess(),
				'position-h' => (new Record(InseeBanPositionHistory::class, 'position-h', BaseYii::t('InseeBanModule.Module', 'Position History')))->enableFullAccess(),
				'postal-area' => (new Record(InseeBanPostalArea::class, 'postal-area', BaseYii::t('InseeBanModule.Module', 'Postal Area')))->enableFullAccess(),
				'postal-area-h' => (new Record(InseeBanPostalArea::class, 'postal-area-h', BaseYii::t('InseeBanModule.Module', 'Postal Area History')))->enableFullAccess(),
				'type-voie' => (new Record(InseeBanTypeVoie::class, 'type-voie', BaseYii::t('InseeBanModule.Module', 'Type Voie')))->enableFullAccess(),
				'source' => (new Record(InseeBanSource::class, 'source', BaseYii::t('InseeBanModule.Module', 'Source')))->enableFullAccess(),
			]),
		];
	}
	
}
