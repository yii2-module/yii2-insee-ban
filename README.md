# yii2-module/yii2-module-ban

A module that structures the data of the Base des Adresses Nationales (fr : BAN)
from the INSEE and others.

![coverage](https://gitlab.com/yii2-module/yii2-insee-ban/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/yii2-module/yii2-insee-ban/badges/master/coverage.svg?style=flat-square)

## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-module/yii2-insee-ban ^8`


## Configuration

This module needs the following components to be set at the configuration level:

- 'db_insee_ban' should be a `\yii\db\Connection`

If you already have a database connection, you may use the following trick :

`'db_insee_ban' => function() { return \Yii::$app->get('db'); },`

where 'db' is the id of your database connection.


This module uses the following parameters to be set at the configuration level:

- `InseeBanModule::tempDirPath` may be configured to a valid directory
or alias known by Yii. Its default is `@app/runtime/insee-ban`, which
is automatically created if it does not exist. This parameter is used to
download and uncompress the csv source files of the BAN, and should be cleared
occasionnally.


Then the module should be configured as follows (in `console.php` or `web.php`) :

```php
$config = [
	...
	'modules' => [
		...
		'insee-ban' => [
			'class' => 'Yii2Module\Yii2InseeBan\InseeBanModule',
		],
		...
	],
	...
];
```


## License

MIT (See [license file](LICENSE))
