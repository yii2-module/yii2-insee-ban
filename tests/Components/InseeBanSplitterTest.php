<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-ban library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpoint;
use PhpExtended\Logger\BasicConsoleLogger;
use PhpExtended\Slugifier\SlugifierFactory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Yii2Module\Yii2InseeBan\Components\InseeBanSplitter;

/**
 * @author Anastaszor
 * @covers \Yii2Module\Yii2InseeBan\Components\InseeBanSplitter
 * 
 * @internal
 * 
 * @small
 */
class InseeBanSplitterTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InseeBanSplitter
	 */
	protected InseeBanSplitter $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testAllee() : void
	{
		$split = $this->_object->split('ALLEE DES VIGNES DE BEAUREGARD');
		$this->assertEquals('ALL', $split->getTypeVoie()->getCode());
		$this->assertEquals('DES VIGNES DE BEAUREGARD', $split->getRemaining());
	}
	
	public function testAncien() : void
	{
		$split = $this->_object->split('ANCIEN COUVENT DE RIVES');
		$this->assertEquals('LD', $split->getTypeVoie()->getCode());
		$this->assertEquals('ANCIEN COUVENT DE RIVES', $split->getRemaining());
	}
	
	public function testBois() : void
	{
		$split = $this->_object->split('Bois Girard');
		$this->assertEquals('LD', $split->getTypeVoie()->getCode());
		$this->assertEquals('Bois Girard', $split->getRemaining());
	}
	
	public function testChemin() : void
	{
		$split = $this->_object->split('Chemin des Vignes');
		$this->assertEquals('CHE', $split->getTypeVoie()->getCode());
		$this->assertEquals('des Vignes', $split->getRemaining());
	}
	
	public function testChemin2() : void
	{
		$split = $this->_object->split('CHEMIN DU REPET');
		$this->assertEquals('CHE', $split->getTypeVoie()->getCode());
		$this->assertEquals('DU REPET', $split->getRemaining());
	}
	
	public function testChemin3() : void
	{
		$split = $this->_object->split('chemin fleuri');
		$this->assertEquals('CHE', $split->getTypeVoie()->getCode());
		$this->assertEquals('fleuri', $split->getRemaining());
	}
	
	public function testHameau() : void
	{
		$split = $this->_object->split('Hameau MAISON ROUGE');
		$this->assertEquals('HAM', $split->getTypeVoie()->getCode());
		$this->assertEquals('MAISON ROUGE', $split->getRemaining());
	}
	
	public function testLieuDit() : void
	{
		$split = $this->_object->split('Lieu-dit Lusclade');
		$this->assertEquals('LD', $split->getTypeVoie()->getCode());
		$this->assertEquals('Lusclade', $split->getRemaining());
	}
	
	public function testPresDe() : void
	{
		$split = $this->_object->split('PRES DE TOUCHERONDE');
		$this->assertEquals('LD', $split->getTypeVoie()->getCode());
		$this->assertEquals('PRES DE TOUCHERONDE', $split->getRemaining());
	}
	
	public function testMarche() : void
	{
		$split = $this->_object->split('Marché DE LA PROUDOUMETTE');
		$this->assertEquals('MAR', $split->getTypeVoie()->getCode());
		$this->assertEquals('DE LA PROUDOUMETTE', $split->getRemaining());
	}
	
	public function testRoute() : void
	{
		$split = $this->_object->split('route des charpentiers');
		$this->assertEquals('RTE', $split->getTypeVoie()->getCode());
		$this->assertEquals('des charpentiers', $split->getRemaining());
	}
	
	public function testRoute2() : void
	{
		$split = $this->_object->split('ROUTE DE MEYMONT');
		$this->assertEquals('RTE', $split->getTypeVoie()->getCode());
		$this->assertEquals('DE MEYMONT', $split->getRemaining());
	}
	
	public function testRue() : void
	{
		$split = $this->_object->split('rue chivalié');
		$this->assertEquals('RUE', $split->getTypeVoie()->getCode());
		$this->assertEquals('chivalié', $split->getRemaining());
	}
	
	public function testRue2() : void
	{
		$split = $this->_object->split('R DE HABLE');
		$this->assertEquals('RUE', $split->getTypeVoie()->getCode());
		$this->assertEquals('DE HABLE', $split->getRemaining());
	}
	
	public function testRue3() : void
	{
		$split = $this->_object->split('rue de saint martin');
		$this->assertEquals('RUE', $split->getTypeVoie()->getCode());
		$this->assertEquals('de saint martin', $split->getRemaining());
	}
	
	public function testImpasse() : void
	{
		$split = $this->_object->split('impasse du porche');
		$this->assertEquals('IMP', $split->getTypeVoie()->getCode());
		$this->assertEquals('du porche', $split->getRemaining());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		if(!\is_dir('/tmp/__ban__'))
		{
			\mkdir('/tmp/__ban__');
		}
		
		$this->_object = new InseeBanSplitter(
			\iterator_to_array(
				(new ApiFrInseeBanEndpoint(
					'/tmp/__ban__',
					$this->getMockForAbstractClass(ClientInterface::class),
				))->getTypeVoieIterator(),
			),
			new SlugifierFactory(),
			new BasicConsoleLogger(3),
		);
	}
	
}
